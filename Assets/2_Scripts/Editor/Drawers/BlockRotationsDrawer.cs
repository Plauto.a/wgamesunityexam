﻿using UnityEditor;
using UnityEngine;

/// <summary>
///This PropertyDrawer is to basically give a better interface for adding and removing elements from the BlockRotations list.
/// </summary>
[CustomPropertyDrawer(typeof(BlockRotations), true)]
public class BlockRotationsDrawer : PropertyDrawer
{
    private const int addRotButtonHeigh = 18;
    private const int cellRotationSize = 68;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if (property.serializedObject.isEditingMultipleObjects)
        {
            return;
        }
        SerializedProperty rotationsProp = property.FindPropertyRelative("rotations");
        if (GUI.Button(new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight), new GUIContent("Add Rotation")))
        {
            rotationsProp.InsertArrayElementAtIndex(rotationsProp.arraySize);
        }
        position.y += addRotButtonHeigh;

        for (int i = 0; i < rotationsProp.arraySize; i++)
        {
            SerializedProperty aCellRotationProp = rotationsProp.GetArrayElementAtIndex(i);

            EditorGUI.PropertyField(position, aCellRotationProp, label);

            if (GUI.Button(new Rect(position.x + cellRotationSize, position.y, position.width - cellRotationSize, EditorGUIUtility.singleLineHeight), new GUIContent("Remove Rotation")))
            {
                rotationsProp.DeleteArrayElementAtIndex(i);
            }

            position.y += cellRotationSize;
        }
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        SerializedProperty rotationsProp = property.FindPropertyRelative("rotations");
        if (rotationsProp.arraySize > 0)
        {
            return (cellRotationSize * (rotationsProp.arraySize + 1)) + addRotButtonHeigh + 5;
        }
        else
        {
            return addRotButtonHeigh + 5;
        }
    }
}
