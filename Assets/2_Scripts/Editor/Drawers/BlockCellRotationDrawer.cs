﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// This PropertyDrawer created a 4x4 Toggle group representing the 16 cells of a Block.
/// This Allows designers to easily and visually configure how blocks looks at any rotation.
/// </summary>
[CustomPropertyDrawer(typeof(BlockCellRotation), true)]
public class BlockCellRotationDrawer : PropertyDrawer
{
    const float checkSize = 16;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        SerializedProperty positionsProp = property.FindPropertyRelative("positions");

        //An array to represent the 16 (4x4) cells on the blocks
        bool[] isMarked = new bool[16];
        for (int i = 0; i < positionsProp.arraySize; i++)
        {
            isMarked[positionsProp.GetArrayElementAtIndex(i).intValue] = true;
        }

        Rect boxRect = EditorGUI.IndentedRect(position);
        EditorGUI.HelpBox(new Rect(boxRect.x, boxRect.y, checkSize*4, checkSize * 4), "", MessageType.None);
        
        //The +1 is just for visual alignment withing the Box
        Rect check12 = new Rect(position.x + 1, position.y, checkSize, checkSize);
        Rect check13 = new Rect(position.x + 1 + checkSize, position.y, checkSize, checkSize);
        Rect check14 = new Rect(position.x + 1 + checkSize*2, position.y, checkSize, checkSize);
        Rect check15 = new Rect(position.x + 1 + checkSize*3, position.y, checkSize, checkSize);
        position.y += checkSize;
        Rect check08 = new Rect(position.x + 1, position.y, checkSize, checkSize);
        Rect check09 = new Rect(position.x + 1 + checkSize, position.y, checkSize, checkSize);
        Rect check10 = new Rect(position.x + 1 + checkSize * 2, position.y, checkSize, checkSize);
        Rect check11 = new Rect(position.x + 1 + checkSize * 3, position.y, checkSize, checkSize);
        position.y += checkSize;
        Rect check04 = new Rect(position.x + 1, position.y, checkSize, checkSize);
        Rect check05 = new Rect(position.x + 1 + checkSize, position.y, checkSize, checkSize);
        Rect check06 = new Rect(position.x + 1 + checkSize * 2, position.y, checkSize, checkSize);
        Rect check07 = new Rect(position.x + 1 + checkSize * 3, position.y, checkSize, checkSize);
        position.y += checkSize;
        Rect check00 = new Rect(position.x + 1, position.y, checkSize, checkSize);
        Rect check01 = new Rect(position.x + 1 + checkSize, position.y, checkSize, checkSize);
        Rect check02 = new Rect(position.x + 1 + checkSize * 2, position.y, checkSize, checkSize);
        Rect check03 = new Rect(position.x + 1 + checkSize * 3, position.y, checkSize, checkSize);

        EditorGUI.BeginChangeCheck();
        //The index on the BlockCellRotation list goes from bottom Left to Top Right, the process of
        //buiding the checkbox goes from Top Left to Bottom Right, thats why there some math in the indexes.
        isMarked[12] = EditorGUI.Toggle(check12, GUIContent.none, isMarked[12]);
        isMarked[13] = EditorGUI.Toggle(check13, GUIContent.none, isMarked[13]);
        isMarked[14] = EditorGUI.Toggle(check14, GUIContent.none, isMarked[14]);
        isMarked[15] = EditorGUI.Toggle(check15, GUIContent.none, isMarked[15]);
        isMarked[ 8] = EditorGUI.Toggle(check08, GUIContent.none, isMarked[ 8]);
        isMarked[ 9] = EditorGUI.Toggle(check09, GUIContent.none, isMarked[ 9]);
        isMarked[10] = EditorGUI.Toggle(check10, GUIContent.none, isMarked[10]);
        isMarked[11] = EditorGUI.Toggle(check11, GUIContent.none, isMarked[11]);
        isMarked[ 4] = EditorGUI.Toggle(check04, GUIContent.none, isMarked[ 4]);
        isMarked[ 5] = EditorGUI.Toggle(check05, GUIContent.none, isMarked[ 5]);
        isMarked[ 6] = EditorGUI.Toggle(check06, GUIContent.none, isMarked[ 6]);
        isMarked[ 7] = EditorGUI.Toggle(check07, GUIContent.none, isMarked[ 7]);
        isMarked[ 0] = EditorGUI.Toggle(check00, GUIContent.none, isMarked[ 0]);
        isMarked[ 1] = EditorGUI.Toggle(check01, GUIContent.none, isMarked[ 1]);
        isMarked[ 2] = EditorGUI.Toggle(check02, GUIContent.none, isMarked[ 2]);
        isMarked[ 3] = EditorGUI.Toggle(check03, GUIContent.none, isMarked[ 3]);

        //We only update the prefab list if there is a change in one of the togles, helps on editor performance
        if (EditorGUI.EndChangeCheck()) {
            positionsProp.ClearArray();
            int counter = 0;
            for (int i = 0; i < isMarked.Length; i++)
            {
                if (isMarked[i] == true)
                {
                    positionsProp.InsertArrayElementAtIndex(counter);
                    positionsProp.GetArrayElementAtIndex(counter).intValue = i;
                    counter++;
                }
            }
        }
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return checkSize * 4;
    }
}
