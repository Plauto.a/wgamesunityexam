﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// **Bonus Item from specification**
/// This Observer checks changes in the Aspect Ratio Filter, and adjust the
/// scale of aspect ratio sensitive components on the scene.
/// </summary>
[ExecuteInEditMode]
public class AspectRatioObserver : MonoBehaviour {

    [Header("Events Owned")]
    [SerializeField]
    private SO_DataEventFloat aspectRatioChangedEvent;

    [Header("Config")]
    [SerializeField]
    AspectRatioFitter filter;

    /// <summary>Need direct connection so it works in EditorMode </summary>
    [SerializeField]
    List<RectTransform> targets;

    [SerializeField]
    private float scaleAdjust;

    [SerializeField]
    private float positionAdjust;


    private void Update()
    {
        scaleAdjust = filter.aspectRatio * 2.04081f;
        for (int i = 0; i < targets.Count; i++)
        {
            targets[i].localScale = new Vector3(scaleAdjust, 1f, 1f);
            if (Application.isPlaying)
            {
                aspectRatioChangedEvent.Raise(scaleAdjust);
            }
        }
    }
}
