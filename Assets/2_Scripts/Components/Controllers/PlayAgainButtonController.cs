﻿using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Controller that responds to the PlayAgain Button pressed. Its sends an Event informing
/// the button was pressed, and listen to the FadeOutFinished Event to finish so it can
/// reload the scene.
/// </summary>
public class PlayAgainButtonController : MonoBehaviour
{
    [Header("Event Listeners")]
    [SerializeField]
    DataEventListener onFadeOutFinished;

    [Header("Events Owned")]
    [SerializeField]
    SO_DataEvent playAgainButtonPressedEvent;

    private void OnEnable()
    {
        onFadeOutFinished.Enable(this, ReloadScene);
    }

    private void OnDisable()
    {
        onFadeOutFinished.Disable();
    }

    public void PlayAgainButtonPressed()
    {
        playAgainButtonPressedEvent.Raise();
    }

    private void ReloadScene()
    {
        SceneManager.LoadScene("TetrisScene");
    }
}
