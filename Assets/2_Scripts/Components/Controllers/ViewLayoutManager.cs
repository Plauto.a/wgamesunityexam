﻿using UnityEngine;

/// <summary>
/// Manager responsable for adjusting the correct ViewPort Layout depending on the aspect ratio.
/// ps. Can't depend on events due to Execute on edit mode.
/// </summary>
[ExecuteInEditMode]
public class ViewLayoutManager : MonoBehaviour
{
    [Header("Config")]
    [SerializeField]
    private RectTransform tetrisContainer;
    [SerializeField]
    private RectTransform rightSidePanel;
    [SerializeField]
    private RectTransform nextBlockPanel;
    [SerializeField]
    private RectTransform scorePanel;

    [Header("Layouts")]
    [SerializeField]
    private SO_ViewPortLayout layoutLandscape;
    [SerializeField]
    private SO_ViewPortLayout layoutPortrait;

    //private Vector3 tetrisAnchorLand = new Vector3(0, 0, 0);
    //private Vector3 tetrisScaleLand = new Vector3(1, 1, 1);
    //private Vector3 tetrisAnchorPort = new Vector3(0, -157, 0);
    //private Vector3 tetrisScalePort = new Vector3(1, .760f, 1);

    //private Vector3 rightAnchorLand = new Vector3(494, 0, 0);
    //private Vector3 rightSizeLand = new Vector3(320, 1334, 0);
    //private Vector3 rightAnchorPort = new Vector3(0, 503, 0);
    //private Vector3 rightSizePort = new Vector3(693, 310, 0);

    //private Vector3 nextAnchorLand = new Vector3(0, 509, 0);
    //private Vector3 nextSizeLand = new Vector3(275, 275, 0);
    //private Vector3 nextAnchorPort = new Vector3(-189, 3, 0);
    //private Vector3 nextSizePort = new Vector3(275, 275, 0);

    //private Vector3 scoreAnchorLand = new Vector3(0, 308, 0);
    //private Vector3 scoreSizeLand = new Vector3(275, 100, 0);
    //private Vector3 scoreAnchorPort = new Vector3(139, 0, 0);
    //private Vector3 scoreSizePort = new Vector3(275, 100, 0);

    [SerializeField]
    private float currentAspectRatio;

    [SerializeField]
    private bool isPortraitMode;

    private Camera theCam;

    private void Update()
    {
        if (theCam == null)
        {
            theCam = Camera.main;
        }
        currentAspectRatio = theCam.aspect;
        if (currentAspectRatio < 1 && !isPortraitMode)
        {
            isPortraitMode = true;
            ChangeViewMode();
        }
        else if (currentAspectRatio > 1 && isPortraitMode)
        {
            isPortraitMode = false;
            ChangeViewMode();
        }
    }

    private void ChangeViewMode()
    {
        if (isPortraitMode)
        {
            tetrisContainer.anchoredPosition = layoutPortrait.tetrisAnchor;
            tetrisContainer.localScale = layoutPortrait.tetrisScale;

            rightSidePanel.anchoredPosition = layoutPortrait.rightAnchor;
            rightSidePanel.sizeDelta = layoutPortrait.rightSize;

            nextBlockPanel.anchoredPosition = layoutPortrait.nextAnchor;
            nextBlockPanel.sizeDelta = layoutPortrait.nextSize;

            scorePanel.anchoredPosition = layoutPortrait.scoreAnchor;
            scorePanel.sizeDelta = layoutPortrait.scoreSize;
        }
        else
        {
            tetrisContainer.anchoredPosition = layoutLandscape.tetrisAnchor;
            tetrisContainer.localScale = layoutLandscape.tetrisScale;

            rightSidePanel.anchoredPosition = layoutLandscape.rightAnchor;
            rightSidePanel.sizeDelta = layoutLandscape.rightSize;

            nextBlockPanel.anchoredPosition = layoutLandscape.nextAnchor;
            nextBlockPanel.sizeDelta = layoutLandscape.nextSize;

            scorePanel.anchoredPosition = layoutLandscape.scoreAnchor;
            scorePanel.sizeDelta = layoutLandscape.scoreSize;
        }
    }
}
