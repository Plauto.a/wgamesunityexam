﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controller responsable to set all cells with the correct sprite and to animate
/// the alpha change of the dissolve effect.
/// (The inflate effect is handler by the child Cell controller)
/// </summary>
public class LineDestroyedFXController : MonoBehaviour
{
    [Header("Config")]
    [SerializeField]
    private List<FXCellController> fxCells;

    [SerializeField]
    private CanvasGroup canvasGroup;

	public void BeginEffect(List<CellController> originalCells)
    {
        transform.position = new Vector3(transform.position.x, originalCells[0].transform.position.y, transform.localPosition.z);
        for (int i = 0; i < fxCells.Count; i++)
        {
            fxCells[i].SetCell(originalCells[i].sprite);
        }
        canvasGroup.alpha = 1;
        StartCoroutine(FadeCanvasGroup());
    }

    private IEnumerator FadeCanvasGroup()
    {
        float i = 1;
        while (i > 0)
        {
            canvasGroup.alpha = i;
            i -= .1f;
            yield return new WaitForSeconds(0.03f);
        }
        canvasGroup.alpha = 0;
    }
}
