﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Controller responsable for muting and unmuting.
/// </summary>
public class MuteMusicButtonController : MonoBehaviour {

    [Header("Events Owned")]
    /// <summary> Event Raised when a line gets destroyed.</summary>
    [SerializeField]
    private SO_DataEvent muteMusicChangedEvent;

    [Header("Config")]
    [SerializeField]
    private Image muteButtonImage;

    [SerializeField]
    private Sprite mutedSprite;

    [SerializeField]
    private Sprite notMutedSprite;

    private void Start()
    {
        if (PlayerPrefs.GetInt("Mute", 0) == 0)
        {
            muteButtonImage.sprite = notMutedSprite;
        }
        else
        {
            muteButtonImage.sprite = mutedSprite;
        }
    }

    public void OnMuteMusicClicked()
    {
        if (PlayerPrefs.GetInt("Mute", 0) == 0)
        {
            PlayerPrefs.SetInt("Mute", 1);
            muteButtonImage.sprite = mutedSprite;
        }
        else
        {
            PlayerPrefs.SetInt("Mute", 0);
            muteButtonImage.sprite = notMutedSprite;
        }
        muteMusicChangedEvent.Raise();
    }
}
