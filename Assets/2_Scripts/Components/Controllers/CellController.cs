﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Cell Controller responsable for setting the the sprite and disabling the Image component when not in use of
/// each individual cell in the game (GridCells, BlockCells and FXCells) the cell is the basic unit that compound 
/// the blocks and the grid.
/// </summary>
[RequireComponent(typeof(Image))]
public class CellController : MonoBehaviour
{
    [Header("Config")]
    [SerializeField]
    private bool isEmpty;
    public bool IsEmpty { get { return isEmpty; }}

    [SerializeField]
    private Image cellImage;

    public Sprite sprite { get { return cellImage.sprite; } }

    protected virtual void Awake()
    {
        cellImage = GetComponent<Image>();
        cellImage.color = Color.white;
        cellImage.enabled = false;
        isEmpty = true;
    }

    public virtual void SetCell(Sprite sprite)
    {
        cellImage.sprite = sprite;
        cellImage.enabled = true;
        isEmpty = false;
    }

    public virtual void ClearCell()
    {
        cellImage.enabled = false;
        isEmpty = true;
    }
}
