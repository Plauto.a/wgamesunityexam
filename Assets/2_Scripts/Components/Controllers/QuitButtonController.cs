﻿using UnityEngine;

/// <summary>
/// Quit game
/// </summary>
public class QuitButtonController : MonoBehaviour {

	public void QuitButtonPressed()
    {
        Application.Quit();
    }
}
