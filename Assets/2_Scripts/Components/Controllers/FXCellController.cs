﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Subclass to the Cell Controller responsable for the cells that are just gona play the dissolve effect.
/// It overrides the SetCell to start a coroutine that will inflate the cell to 50% bigger.
/// (The alpha portion of the effect is handled by the parent controller)
/// </summary>
public class FXCellController : CellController
{
    public override void SetCell(Sprite sprite)
    {
        base.SetCell(sprite);
        StartCoroutine(Inflate());
    }

    private IEnumerator Inflate()
    {
        float i = 1;
        while (i < 1.5)
        {
            transform.localScale = new Vector3(i, i, 1);
            i += .05f;
            yield return new WaitForSeconds(0.03f);
        }
        base.ClearCell();
    }
}
