﻿using UnityEngine;

/// <summary>
/// Controller that listen to the NextBlockChosen event that when Raised passes
/// the BlockController choosen as the NEXT block to spawn. The controller creates a 
/// new Instance of this block, and set it as PreviewMode ON, witch means it won't
/// listen to any input or other DataEvent, is for display only. Gets destroyed when
/// the next one arrives.
/// </summary>
public class NextBlockPanelController : MonoBehaviour
{
    [Header("Event Listeners")]
    [SerializeField]
    private DataEventBlockControllerListener onNextBlockChosen;

    [Header("Config")]
    [SerializeField]
    private RectTransform previewLocation;

    private GameObject previewPiece;

    private void OnEnable()
    {
        onNextBlockChosen.Enable(this, NextPieceChosenHandler);
    }

    private void OnDisable()
    {
        onNextBlockChosen.Disable();
    }
    
    private void NextPieceChosenHandler(BlockController nextController)
    {
        if (previewPiece != null)
        {
            Destroy(previewPiece);
        }
        BlockController newBlock = Instantiate(nextController, previewLocation);
        newBlock.SetPreviewMode();
        previewPiece = newBlock.gameObject;
    }
}
