﻿using UnityEngine;
using TMPro;

/// <summary>
/// Controller that listen to score changes and update the Text on the screen
/// </summary>
public class ScorePanelController : MonoBehaviour
{
    [Header("Event Listeners")]
    [SerializeField]
    private DataEventIntListener onScoreChanged;

    [Header("Config")]
    [SerializeField]
    private TextMeshProUGUI scoreText;

    private void OnEnable()
    {
        onScoreChanged.Enable(this, OnScoreChangedHandler);
    }

    private void OnScoreChangedHandler(int currentScore)
    {
        scoreText.text = currentScore.ToString();
    }
}
