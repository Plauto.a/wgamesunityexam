﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Controller responsable for fading In and Out the ViewPort CanvasGroup when the stage loads in and out.
/// This alows for the black screen fade effect that hides the scene reload.
/// </summary>
public class ViewPortFadeController : MonoBehaviour
{
    [Header("Event Listeners")]
    [SerializeField]
    private DataEventListener onFadeOutListener;

    [Header("Events Owned")]
    [SerializeField]
    private SO_DataEvent fadeOutFinishedEvent;

    [Header("Config")]
    [SerializeField]
    private CanvasGroup fadeCanvasGroup;

    private void OnEnable()
    {
        fadeCanvasGroup.alpha = 1;
        onFadeOutListener.Enable(this, FadeOut);
        FadeIn();
    }

    private void OnDisable()
    {
        onFadeOutListener.Disable();
    }

    private void FadeIn()
    {
        StartCoroutine(FadeCanvasGroup(true));
    }

    private void FadeOut()
    {
        StartCoroutine(FadeCanvasGroup(false));
    }

    private IEnumerator FadeCanvasGroup(bool isFadeIn)
    {
        if (isFadeIn)
        {
            float i = 1;
            while (i > 0)
            {
                fadeCanvasGroup.alpha = i;
                i -= .05f;
                yield return new WaitForSeconds(0.05f);
            }
            fadeCanvasGroup.alpha = 0;
        }
        else
        {
            float i = 0;
            while (i < 1)
            {
                fadeCanvasGroup.alpha = i;
                i += .05f;
                yield return new WaitForSeconds(0.05f);
            }
            fadeCanvasGroup.alpha = 1;
            fadeOutFinishedEvent.Raise();
        }
    }
}
