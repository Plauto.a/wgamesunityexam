﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controller responsable for organizing and Invoking the Disolve visual effect
/// when a Line gets destroyed.
/// </summary>
public class VisualFXController : MonoBehaviour
{
    [Header("Event Listeners")]
    [SerializeField]
    private DataEventVisualFXListener onVisualFXRequest;

    [Header("Config")]
    [SerializeField]
    private List<LineDestroyedFXController> visualDestroyedFXControllers;

    private void OnEnable()
    {
        onVisualFXRequest.Enable(this, FXRequestHandler);
    }

    private void OnDisable()
    {
        onVisualFXRequest.Disable();
    }

    private void FXRequestHandler(VisualFXInfo fxInfo)
    {
        for (int i = 0; i < fxInfo.cells.Count; i+=10)
        {
            LineDestroyedFXController aLineFX = visualDestroyedFXControllers[i / 10];
            aLineFX.BeginEffect(fxInfo.cells.GetRange(i, 10));
        }
    }
}
