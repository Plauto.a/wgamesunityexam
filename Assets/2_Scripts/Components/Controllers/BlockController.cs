﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Block controller is responsable for controlling and destroying the blocks.
/// 
/// It listen to the Input events and the step events, updates the block positions with
/// the help of the gridController. It does all validation for the movements.
/// </summary>
public class BlockController : MonoBehaviour
{
    [Header("Event Listeners")]
    [SerializeField]
    private DataEventIntListener onStepListener;

    [SerializeField]
    private DataEventListener onInputRotateClockwise;

    [SerializeField]
    private DataEventListener onInputRotateCounterclockwise;

    [SerializeField]
    private DataEventListener onInputMoveRight;

    [SerializeField]
    private DataEventListener onInputMoveLeft;

    [SerializeField]
    private DataEventListener onInputMoveDown;

    [SerializeField]
    private DataEventFloatListener onAspectRatioChanged;

    [Header("Events Owned")]

    /// <summary>  Event Raised when the block will get destroyed. </summary>
    [SerializeField]
    private SO_DataEvent blockDestroyedEvent;

    /// <summary>  Event Raised when the block successfully moded. </summary>
    [SerializeField]
    private SO_DataEvent blockMovedEvent;

    /// <summary>  Event Raised when the block successfully rotated. </summary>
    [SerializeField]
    private SO_DataEvent blockRotatedEvent;

    [Header("Config")]
    [SerializeField]
    private SO_StageConfigInfo stageConfig;

    [SerializeField]
    private Sprite blockCellSprite;

    [SerializeField]
    private List<CellController> blockCells;

    [SerializeField]
    private Vector2Int blockXYPosition;
    public Vector2Int BlockXYPosition { get { return blockXYPosition; } }

    public bool isPreview;
    public GridController gridCtrl;

    [Header("Config - Rotations")]
    [SerializeField]
    private BlockRotations blockRotations;

    private int currentRotation;
    private bool movedInCurrentStep;

    private const int blockSize = 4;

    /// <summary>
    /// When set as PreviewMode, the pivot is changed to the center and its localPosition to zero.
    /// Also disable all event listeners. Since it will be used for display only.
    /// </summary>
    public void SetPreviewMode()
    {
        isPreview = true;
        (transform as RectTransform).pivot = new Vector2(.5f,.5f);
        (transform as RectTransform).localPosition = Vector3.zero;
        OnDisable();
    }

    /// <summary>
    /// Enable all Listeners passing itself and the handler for the event.
    /// </summary>
    private void OnEnable()
    {
        onStepListener.Enable(this, OnStepHandler);
        onInputRotateClockwise.Enable(this, TryRotateClockwiseHandler);
        onInputRotateCounterclockwise.Enable(this, TryRotateCounterclockwiseHandler);
        onInputMoveLeft.Enable(this, TryMoveLeftHandler);
        onInputMoveRight.Enable(this, TryMoveRightHandler);
        onInputMoveDown.Enable(this, TryMoveDownHandler);
        onAspectRatioChanged.Enable(this, AspectRatioChangedHandler);
    }

    /// <summary>
    /// Disable all event listeners.
    /// </summary>
    private void OnDisable()
    {
        onStepListener.Disable();
        onInputRotateClockwise.Disable();
        onInputRotateCounterclockwise.Disable();
        onInputMoveLeft.Disable();
        onInputMoveRight.Disable();
        onInputMoveDown.Disable();
        onAspectRatioChanged.Disable();
    }

    private void Start()
    {
        if (!isPreview)
        {
            UpdateBlockPosition();
        }
        for (int i = 0; i < blockCells.Count; i++)
        {
            blockCells[i].ClearCell();
        }
        UpdateCells();
    }

    /// <summary>
    /// On each steps, if is a valid movement, decrease one position.
    /// </summary>
    /// <param name="curStep"></param>
    private void OnStepHandler(int curStep)
    {
        if (ValidateNextPosition(new Vector2Int(0,-1)))
        {
            blockXYPosition.y--;
            UpdateBlockPosition();
            //In case the cells may be ABOVE the grid (just spawned) always update the cells.
            if (blockXYPosition.y > GridController.gridSizeY - blockSize - 1)
            {
                UpdateCells();
            }
        }
        movedInCurrentStep = false;
    }

    private void TryRotateClockwiseHandler()
    {
        if (ValidateNextRotation(true))
        {
            currentRotation++;
            UpdateCells();
            blockRotatedEvent.Raise();
        }
    }

    private void TryRotateCounterclockwiseHandler()
    {
        if (ValidateNextRotation(false))
        {
            currentRotation = currentRotation == 0 ? (blockRotations.rotations.Count - 1) : currentRotation - 1;
            UpdateCells();
            blockRotatedEvent.Raise();
        }
    }

    private void TryMoveLeftHandler()
    {
        if (!stageConfig.HorizontalMoveLimited || (stageConfig.HorizontalMoveLimited && !movedInCurrentStep))
        {
            if (ValidateNextPosition(new Vector2Int(-1, 0)))
            {
                movedInCurrentStep = true;
                blockXYPosition.x--;
                UpdateBlockPosition();
                blockMovedEvent.Raise();
            }
        }
    }

    private void TryMoveRightHandler()
    {
        if (!stageConfig.HorizontalMoveLimited || (stageConfig.HorizontalMoveLimited && !movedInCurrentStep))
        {
            if (ValidateNextPosition(new Vector2Int(1, 0)))
            {
                movedInCurrentStep = true;
                blockXYPosition.x++;
                UpdateBlockPosition();
                blockMovedEvent.Raise();
            }
        }
    }

    private void TryMoveDownHandler()
    {
        if (ValidateNextPosition(new Vector2Int(0, -1)))
        {
            blockMovedEvent.Raise();
        }
    }

    private void AspectRatioChangedHandler(float scaleX)
    {
        UpdateBlockPosition();
    }

    /// <summary>
    /// Gets the cells XY positions giving a certain Rotation.
    /// </summary>
    /// <param name="theRotation">Current cell rotation</param>
    /// <returns>List of XY positions with active cells</returns>
    private List<Vector2Int> GetCellsGridPositionsXY(BlockCellRotation theRotation)
    {
        List<Vector2Int> positionsXY = new List<Vector2Int>();
        for (int i = 0; i < theRotation.positions.Count; i++)
        {
            Vector2Int posToCheck = blockXYPosition + new Vector2Int(theRotation.positions[i] % blockSize, theRotation.positions[i] / blockSize);
            positionsXY.Add(posToCheck);
        }
        return positionsXY;
    }

    /// <summary>
    /// Giving a Delta +1/-1 on X or -1 on Y. Return True if the position if valid
    /// Position is valid if no Active Cell is outside the grid (except if it is ABOVE).
    /// </summary>
    /// <param name="delta">The direction of the expected movement</param>
    /// <returns>If is valid position or not.</returns>
    private bool ValidateNextPosition(Vector2Int delta)
    {
        BlockCellRotation aRotation = blockRotations.rotations[(currentRotation % blockRotations.rotations.Count)];
        for (int i = 0; i < aRotation.positions.Count; i++)
        {
            Vector2Int posToCheck = blockXYPosition + new Vector2Int(aRotation.positions[i] % blockSize, aRotation.positions[i] / blockSize) + delta;
            if (posToCheck.y < 0)
            {
                gridCtrl.SetCellsAtPositions(GetCellsGridPositionsXY(aRotation), blockCellSprite);
                blockDestroyedEvent.Raise();
                Destroy(gameObject);
                return false;
            }
            else
            {
                CellController gridCell = gridCtrl.GetCellAtPosition(posToCheck);
                if (gridCell == null)
                {
                    if (posToCheck.y < GridController.gridSizeY)
                    {
                        return false;
                    }
                }
                else if (!gridCell.IsEmpty)
                {
                    if (delta.x == 0)
                    {
                        gridCtrl.SetCellsAtPositions(GetCellsGridPositionsXY(aRotation), blockCellSprite);
                        blockDestroyedEvent.Raise();
                        Destroy(gameObject);
                        return false;
                    }
                    return false;
                }
            }
        }
        return true;
    }

    /// <summary>
    /// Tells if a given rotation is valid by checking if all of the active cells
    /// will land inside the Grid. (Above it is accepted).
    /// </summary>
    /// <param name="delta">The direction of the rotation.</param>
    /// <returns>If is valid position or not.</returns>
    private bool ValidateNextRotation(bool isClockwise)
    {
        int nextRotation;
        if (isClockwise)
        {
            nextRotation = currentRotation + 1;
        }
        else
        {
            nextRotation = currentRotation == 0 ? (blockRotations.rotations.Count - 1) : currentRotation-1;
        }
        BlockCellRotation aRotation = blockRotations.rotations[(nextRotation % blockRotations.rotations.Count)];
        for (int i = 0; i < aRotation.positions.Count; i++)
        {
            Vector2Int posToCheck = blockXYPosition + new Vector2Int(aRotation.positions[i] % blockSize, aRotation.positions[i] / blockSize);
            if (posToCheck.y < 0)
            {
                return false;
            }
            else
            {
                CellController gridCell = gridCtrl.GetCellAtPosition(posToCheck);
                if (gridCell == null && posToCheck.y < GridController.gridSizeY)
                {
                    return false;
                }
                else if (gridCell != null && !gridCell.IsEmpty)
                {
                    return false;
                }
            }
        }
        return true;
    }

    private void UpdateBlockPosition()
    {
        transform.position = gridCtrl.GetXCellPositionOfLineY(blockXYPosition);
    }

    /// <summary>
    /// Given the current rotation, Activate the expected cells.
    /// </summary>
    private void UpdateCells()
    {
        BlockCellRotation aRotation = blockRotations.rotations[(currentRotation % blockRotations.rotations.Count)];
        for (int i = 0; i < blockCells.Count; i++)
        {
            if (aRotation.positions.Contains(i))
            {
                Vector2Int posToCheck = blockXYPosition + new Vector2Int(i % blockSize, i / blockSize);
                //If is preview, can ignore the check since it will be outside the grid anyway.
                if (posToCheck.y < GridController.gridSizeY || isPreview)
                {
                    blockCells[i].SetCell(blockCellSprite);
                }
            }
            else
            {
                blockCells[i].ClearCell();
            }
        }
    }

    private void OnValidate()
    {
        if (blockRotations.rotations == null)
        {
            blockRotations.rotations = new List<BlockCellRotation>();
        }
        if (blockRotations.rotations.Count == 0)
        {
            blockRotations.rotations.Add(new BlockCellRotation());
        }

        BlockCellRotation aRotation = blockRotations.rotations[0];
        for (int i = 0; i < blockCells.Count; i++)
        {
            if (aRotation.positions != null && aRotation.positions.Contains(i))
            {
                blockCells[i].SetCell(blockCellSprite);
            }
            else
            {
                blockCells[i].ClearCell();
            }
        }
    }
}
