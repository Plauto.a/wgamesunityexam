﻿using UnityEngine;

/// <summary>
/// Controller responsable to respond to the Click on the StartStage button and Raise the
/// event to all listeners.
/// </summary>
public class StartStageButtonController : MonoBehaviour
{
    [Header("Events Owned")]
    [SerializeField]
    private SO_DataEvent stageStartEvent;

    public void StartGameButtonPressed()
    {
        stageStartEvent.Raise();
    }
}
