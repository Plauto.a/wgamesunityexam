﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Grid Controller responsable for the Cells grid.
/// This controller maintains a List of cells. Since cells with the Image component disabled are very cheap
/// the cells are kepts active, but the Image component gets disabled when the cell is not beeing used.
/// 
/// This allows for the use of the GridLayoutGroup UI Script, which gives an amazing flexibility for altering the aspect ratio and keeping
/// the grid, the Blocks and the Effects correctly align.
/// </summary>
public class GridController : MonoBehaviour
{
    [Header("Event Listeners")]
    [SerializeField]
    private DataEventListener onBlockDestroyed;

    [SerializeField]
    private DataEventFloatListener onAspectRatioChanged;

    [Header("Events Owned")]
    /// <summary> Event Raised when a line gets destroyed.</summary>
    [SerializeField]
    private SO_DataEvent lineDestroyedEvent;

    /// <summary> Event Raised when a line gests detroyed: send a VisualFXInfo as parameter</summary>
    [SerializeField]
    private SO_DataEventVisualFXInfo lineDestroyedFXInfoEvent;

    /// <summary> Event Raised when the game ended</summary>
    [SerializeField]
    private SO_DataEvent stageEndedEvent;

    /// <summary> Event Raised when the game ended: send the score as parameter</summary>
    [SerializeField]
    private SO_DataEventInt stageEndedWithScoreEvent;

    /// <summary> Event Raised when the score changed: send the score as parameter</summary>
    [SerializeField]
    private SO_DataEventInt scoreChangedEvent;

    /// <summary> Event Raised when all lines were checked for beeing completed</summary>
    [SerializeField]
    private SO_DataEvent finishLineCheckEvent;

    [Header("Config")]
    [SerializeField]
    private List<CellController> gridCells = new List<CellController>();

    public float xDistance;
    public float yDistance;

    public const int gridSizeX = 10;
    public const int gridSizeY = 22;

    private int score;

    private void Awake()
    {
        score = 0;
        AspectRatioChangedHandler(1);
    }

    private void OnEnable()
    {
        onBlockDestroyed.Enable(this, OnBlockDestroyedHandler);
        onAspectRatioChanged.Enable(this, AspectRatioChangedHandler);
    }

    private void OnDisable()
    {
        onBlockDestroyed.Disable();
    }
    
    private void AspectRatioChangedHandler(float scaleX)
    {
        if (gridCells[0] != null && gridCells[1] != null && gridCells[10] != null)
        {
            xDistance = Mathf.Abs(gridCells[0].transform.position.x - gridCells[1].transform.position.x);
            yDistance = Mathf.Abs(gridCells[0].transform.position.y - gridCells[10].transform.position.y);
        }    
    }

    /// <summary>
    /// Return the Vector3 cell position in the 'GridSpace' of a given XY position
    /// </summary>
    /// <param name="positionXY">XY cell position</param>
    /// <returns></returns>
    public Vector3 GetXCellPositionOfLineY(Vector2Int positionXY)
    {
        return gridCells[0].transform.position + new Vector3(xDistance * positionXY.x, yDistance * positionXY.y, 0);
    }

    /// <summary>
    /// return the CellController in a given XY position.
    /// </summary>
    /// <param name="positionXY">XY cell position</param>
    /// <returns></returns>
    public CellController GetCellAtPosition(Vector2Int positionXY)
    {
        if (positionXY.x < 0 || positionXY.x >= gridSizeX || positionXY.y < 0 || positionXY.y >= gridSizeY)
        {
            return null;
        }
        return gridCells[positionXY.y * gridSizeX + positionXY.x];
    }

    /// <summary>
    /// Set a List of cells in the given XY positions with the sprite received.
    /// </summary>
    /// <param name="positionsXY">XY positions of the cells</param>
    /// <param name="sprite">Sprite to use</param>
    public void SetCellsAtPositions(List<Vector2Int> positionsXY, Sprite sprite)
    {
        for (int i = 0; i < positionsXY.Count; i++)
        {
            if (positionsXY[i].x >= 0 && positionsXY[i].x < gridSizeX && positionsXY[i].y >= 0 && positionsXY[i].y < gridSizeY)
            {
                gridCells[positionsXY[i].y * gridSizeX + positionsXY[i].x].SetCell(sprite);
            }
            else if(positionsXY[i].y >= gridSizeY)
            {
                stageEndedWithScoreEvent.Raise(score);
                stageEndedEvent.Raise();
            }
        }
    }

    /// <summary>
    /// A Block gets distroyed when it reaches the bottom of the grid or collide with
    /// a Not empty cell. This function handle that event by checking if after a block
    /// gets destroyed, any line got completed and needs to get dissolved as well.
    /// </summary>
    private void OnBlockDestroyedHandler()
    {
        List<int> linesCleared = new List<int>();
        VisualFXInfo fxInfo = new VisualFXInfo();
        fxInfo.cells = new List<CellController>();
        for (int y = 0; y < gridSizeY; y++)
        {
            bool isComplete = true;
            for (int x = 0; x < gridSizeX; x++)
            {
                if (gridCells[y * gridSizeX + x].IsEmpty)
                {
                    isComplete = false;
                    break;
                }
            }
            if (isComplete)
            {
                linesCleared.Add(y);
                for (int x = 0; x < gridSizeX; x++)
                {
                    fxInfo.cells.Add(gridCells[y * gridSizeX + x]);
                    gridCells[y * gridSizeX + x].ClearCell();
                }
                score += 100;
                lineDestroyedEvent.Raise();
            }
        }
        if (linesCleared.Count > 0)
        {
            lineDestroyedFXInfoEvent.Raise(fxInfo);
            scoreChangedEvent.Raise(score);
            SlideBlocksDown(linesCleared);
        }
        finishLineCheckEvent.Raise();
    }

    /// <summary>
    /// Function called when at least one line got dissolved, this will slide the
    /// corresponding blocks down.
    /// </summary>
    /// <param name="linesCleared"></param>
    private void SlideBlocksDown(List<int> linesCleared)
    {
        for (int y = 0; y < gridSizeY; y++)
        {
            int linesToFall = 0;
            for (int j = 0; j < linesCleared.Count; j++)
            {
                if (y > linesCleared[j] && !linesCleared.Contains(y))
                {
                    linesToFall++;
                }
                else {
                    break;
                }
            }
            if (linesToFall > 0)
            {
                for (int x = 0; x < gridSizeX; x++)
                {
                    if (!(gridCells[y * gridSizeX + x].IsEmpty))
                    {
                        gridCells[(y - linesToFall) * gridSizeX + x].SetCell(gridCells[y * gridSizeX + x].sprite);
                        gridCells[y * gridSizeX + x].ClearCell();
                    }
                }
            }
        }
    }
}
