﻿using UnityEngine;
using TMPro;

/// <summary>
/// Controller responsable for enabling and disabling the EndGameViewPort
/// </summary>
public class ViewPortEndGameController : MonoBehaviour
{
    [Header("Event Listeners")]
    [SerializeField]
    private DataEventIntListener onStagedEnded;

    [Header("Config")]
    [SerializeField]
    private GameObject viewPortContainer;

    [SerializeField]
    private TextMeshProUGUI scoreValueText;

    private void OnEnable()
    {
        onStagedEnded.Enable(this, StageEndedEventHandler);
    }

    private void OnDisable()
    {
        onStagedEnded.Disable();
    }
    
    private void StageEndedEventHandler(int score)
    {
        viewPortContainer.SetActive(true);
        scoreValueText.text = score.ToString();
    }
}
