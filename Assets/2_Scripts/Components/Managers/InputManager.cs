﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The Input Manager is able to care for multiple InputSchemes at the same time, this allows 
/// to easily configure the game to, for example, use arrows or WASD or number pad all at the same time.
/// </summary>
public class InputManager : MonoBehaviour
{
    [Header("Event Listeners")]
    [SerializeField]
    private DataEventIntListener onGridStageEnded;

    [Header("Events Owned")]
    [SerializeField]
    private SO_DataEvent inputRotateClockwiseEvent;

    [SerializeField]
    private SO_DataEvent inputRotateCounterclockwiseEvent;

    [SerializeField]
    private SO_DataEvent inputMoveRightEvent;

    [SerializeField]
    private SO_DataEvent inputMoveLeftEvent;

    [SerializeField]
    private SO_DataEvent inputMoveDownEvent;

    [SerializeField]
    private SO_DataEvent inputPauseEvent;

    [SerializeField]
    private SO_DataEventBool inputSpeedUpEvent;

    [Header("Config")]
    [SerializeField]
    private SO_StageConfigInfo stageConfig;

    [SerializeField]
    private List<SO_InputScheme> inputSchemes;

    private bool stageEnded;

    private void OnEnable()
    {
        onGridStageEnded.Enable(this, StageEndedEventHandler);
    }

    private void OnDisable()
    {
        onGridStageEnded.Disable();
    }

    private void StageEndedEventHandler(int score)
    {
        stageEnded = true;
    }

    private void Update()
    {
        if (!stageEnded)
        {
            for (int i = 0; i < inputSchemes.Count; i++)
            {
                if (Input.GetKeyDown(inputSchemes[i].rotateClockwiseKey))
                {
                    inputRotateClockwiseEvent.Raise();
                }
                if (Input.GetKeyDown(inputSchemes[i].rotateCounterclockwiseKey))
                {
                    inputRotateCounterclockwiseEvent.Raise();
                }
                if (stageConfig.HorizontalMoveLimited)
                {
                    if (Input.GetKey(inputSchemes[i].moveRightKey))
                    {
                        inputMoveRightEvent.Raise();
                    }
                    if (Input.GetKey(inputSchemes[i].moveLeftKey))
                    {
                        inputMoveLeftEvent.Raise();
                    }
                    if (Input.GetKey(inputSchemes[i].moveDowntKey))
                    {
                        inputMoveDownEvent.Raise();
                    }
                }
                else
                {
                    if (Input.GetKeyDown(inputSchemes[i].moveRightKey))
                    {
                        inputMoveRightEvent.Raise();
                    }
                    if (Input.GetKeyDown(inputSchemes[i].moveLeftKey))
                    {
                        inputMoveLeftEvent.Raise();
                    }
                    if (Input.GetKeyDown(inputSchemes[i].moveDowntKey))
                    {
                        inputMoveDownEvent.Raise();
                    }
                }

                if (Input.GetKeyDown(inputSchemes[i].pauseKey))
                {
                    inputPauseEvent.Raise();
                }

                if (Input.GetKeyDown(inputSchemes[i].moveDowntKey))
                {
                    inputSpeedUpEvent.Raise(true);
                }
                if (Input.GetKeyUp(inputSchemes[i].moveDowntKey))
                {
                    inputSpeedUpEvent.Raise(false);
                }
            }
        }
    }
}
