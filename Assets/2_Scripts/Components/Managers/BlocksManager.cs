﻿using UnityEngine;

/// <summary>
/// The Manager responsable for spawning new blocks
/// </summary>
public class BlocksManager : MonoBehaviour
{
    [Header("Event Listeners")]
    [SerializeField]
    private DataEventListener onStageStart;

    [SerializeField]
    private DataEventListener onGridFinishLineCheck;

    [Header("Events Owned")]
    [SerializeField]
    private SO_DataEventBlockController nextBlockChosenEvent;

    [Header("Config")]
    [SerializeField]
    private SO_StageConfigInfo stageConfig;

    [SerializeField]
    private GridController gridCtrl;

    private BlockController nextBlock;

    private void OnEnable()
    {
        onGridFinishLineCheck.Enable(this, OnGridFinishLineCheckHandler);
        onStageStart.Enable(this, StageStartHandler);
    }

    private void OnDisable()
    {
        onGridFinishLineCheck.Disable();
        onStageStart.Disable();
    }

    private void StageStartHandler()
    {
        GetNextBlock();  
    }

    private void OnGridFinishLineCheckHandler()
    {
        GetNextBlock();
    }

    private BlockController GetNextBlock()
    {
        if (stageConfig.AvaliableBlocks.Count > 0)
        {
            if (nextBlock == null)
            {
                nextBlock = stageConfig.AvaliableBlocks[UnityEngine.Random.Range(0, stageConfig.AvaliableBlocks.Count)];
            }
            BlockController newBlock = Instantiate(nextBlock, transform);
            newBlock.gridCtrl = gridCtrl;
            nextBlock = stageConfig.AvaliableBlocks[UnityEngine.Random.Range(0, stageConfig.AvaliableBlocks.Count)];
            nextBlockChosenEvent.Raise(nextBlock);
            return newBlock;
        }
        return null;
    }

}
