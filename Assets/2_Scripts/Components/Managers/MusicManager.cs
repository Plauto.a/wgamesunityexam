﻿using System;
using System.Collections;
using UnityEngine;

/// <summary>
/// Responsable for playing the backgroundMusic and Fading in and out
/// </summary>
public class MusicManager : MonoBehaviour
{
    [Header("Event Listeners")]
    [SerializeField]
    private DataEventListener onFadeOut;

    [SerializeField]
    private DataEventListener onMuteMusic;

    [Header("Config")]
    [SerializeField]
    private AudioSource musicAudioSource;

    private void OnEnable()
    {
        onMuteMusic.Enable(this, OnMuteMusicChangeHandler);
        musicAudioSource.volume = 0;
        onFadeOut.Enable(this, FadeOut);
        FadeIn();
    }

    private void OnDisable()
    {
        onMuteMusic.Disable();
        onFadeOut.Disable();
    }

    private void OnMuteMusicChangeHandler()
    {
        int muteState = PlayerPrefs.GetInt("Mute", 0);
        if (muteState == 0)
        {
            musicAudioSource.volume = 1;
        }
        else{
            musicAudioSource.volume = 0;
        }
    }

    private void FadeIn()
    {
        if (PlayerPrefs.GetInt("Mute", 0) == 0)
        {
            StartCoroutine(FadeCanvasGroup(true));
        }
    }

    private void FadeOut()
    {
        if (PlayerPrefs.GetInt("Mute", 0) == 0)
        {
            StartCoroutine(FadeCanvasGroup(false));
        }
    }

    private IEnumerator FadeCanvasGroup(bool isFadeIn)
    {
        if (isFadeIn)
        {
            float i = 0;
            while (i < 1)
            {
                musicAudioSource.volume = i;
                i += .05f;
                yield return new WaitForSeconds(0.04f);
            }
            musicAudioSource.volume = 1;
        }
        else
        {
            float i = 1;
            while (i > 0)
            {
                musicAudioSource.volume = i;
                i -= .05f;
                yield return new WaitForSeconds(0.03f);
            }
            musicAudioSource.volume = 0;
        }
    }
}
