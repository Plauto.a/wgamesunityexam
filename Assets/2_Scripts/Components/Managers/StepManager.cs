﻿using UnityEngine;

/// <summary>
/// This Manager controls the game Steps, Raisin the DataEvent stepEvent every
/// x senconds, the internval is configurable through the stageConfig
/// ScriptableObject file assigned in the project.
/// </summary>
public class StepManager : MonoBehaviour
{
    [Header("Event Listeners")]
    [SerializeField]
    private DataEventListener onStageStart;

    [SerializeField]
    private DataEventBoolListener onInputSpeedUp;

    [SerializeField]
    private DataEventListener onInputPause;

    [SerializeField]
    private DataEventIntListener onGridStageEnded;

    [Header("Events Owned")]
    /// <summary>  Event Raised when a Step happens. </summary>
    [SerializeField]
    private SO_DataEventInt stepEvent;

    [Header("Config")]
    [SerializeField]
    private SO_StageConfigInfo stageConfig;

    private float timer;
    private int currentStep;

    private float timeModifier;
    private float inputModifier;

    private bool paused;
    private bool ended;

    private void OnEnable()
    {
        timeModifier = 1;
        inputModifier = 1;
        paused = true;
        onInputSpeedUp.Enable(this, SpeedUpHandler);
        onInputPause.Enable(this, PauseUnpauseHandler);
        onGridStageEnded.Enable(this, StageEndedHandler);
        onStageStart.Enable(this, StageStartHandler);
    }

    private void OnDisable()
    {
        onInputSpeedUp.Disable();
        onInputPause.Disable();
        onGridStageEnded.Disable();
    }

    void Update ()
    {
        if (!paused && !ended)
        {
            if (timer >= stageConfig.StepTimeInSeconds * timeModifier * inputModifier)
            {
                currentStep++;
                timer = 0;
                stepEvent.Raise(currentStep);
            }
            else
            {
                timer += Time.deltaTime;
            }
        }
	}

    private void StageStartHandler()
    {
        paused = false;
    }

    private void SpeedUpHandler(bool activate)
    {
        inputModifier = activate ? .1f : 1;
    }

    private void PauseUnpauseHandler()
    {
        paused = !paused;
    }

    private void StageEndedHandler(int score)
    {
        ended = true;
    }
}
