﻿using UnityEngine;

/// <summary>
/// ScriptableObject containing one Key scheme that could be used by the game.
/// </summary>
[CreateAssetMenu]
public class SO_InputScheme : ScriptableObject
{
    [Header("Keys")]
    public KeyCode moveRightKey;
    public KeyCode moveLeftKey;
    public KeyCode moveDowntKey;
    public KeyCode rotateClockwiseKey;
    public KeyCode rotateCounterclockwiseKey;
    public KeyCode pauseKey;
}
