﻿using UnityEngine;

/// <summary>
/// This ScriptableObject is a SO_DataEventParamBase subclass that allows you to Raise
/// events that send a BlockController as a parameter.
/// </summary>
[CreateAssetMenu(fileName = "DataEventBlockController", menuName = "DataEvents/Tetris/BlockController")]
public class SO_DataEventBlockController : SO_DataEventParamBase<BlockController>
{
}
