﻿using System.Collections;
using UnityEngine;

/// <summary>
/// A ScriptableObject containing a Layout for the views
/// </summary>
[CreateAssetMenu]
public class SO_ViewPortLayout : ScriptableObject {

    [Header("Tetris Container Parameters")]
    public Vector3 tetrisAnchor = new Vector3(0, 0, 0);
    public Vector3 tetrisScale = new Vector3(1, 1, 1);

    [Header("Right Side Panel Parameters")]
    public Vector3 rightAnchor = new Vector3(494, 0, 0);
    public Vector3 rightSize = new Vector3(320, 1334, 0);

    [Header("NextBlock Panel Parameters")]
    public Vector3 nextAnchor = new Vector3(0, 509, 0);
    public Vector3 nextSize = new Vector3(275, 275, 0);

    [Header("Score Panel Parameters")]
    public Vector3 scoreAnchor = new Vector3(0, 308, 0);
    public Vector3 scoreSize = new Vector3(275, 100, 0);

}
