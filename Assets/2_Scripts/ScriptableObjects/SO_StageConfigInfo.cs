﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ScriptableObject used to group general configuration for the game.
/// </summary>
[CreateAssetMenu]
public class SO_StageConfigInfo : ScriptableObject
{
    [SerializeField]
    private float stepTimeInSeconds;
    public float StepTimeInSeconds { get { return stepTimeInSeconds; }}

    //This limit the horizontal movement with the Step, as it was requested in the specification.
    //However, I like more the gameplay with free Horizontal movement (independent from the Step)
    //by setting this to false, the horizontal moviments is independent from the Step.
    [SerializeField]
    private bool horizontalMoveLimited = true;
    public bool HorizontalMoveLimited { get { return horizontalMoveLimited; } }

    //Blocks that can be chosen
    [SerializeField]
    private List<BlockController> avaliableBlocks;
    public List<BlockController> AvaliableBlocks { get { return avaliableBlocks; } }
}
