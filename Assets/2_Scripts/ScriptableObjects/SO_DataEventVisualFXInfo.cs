﻿using UnityEngine;

/// <summary>
/// This ScriptableObject is a SO_DataEventParamBase subclass that allows you to Raise
/// events that send a VisualFXInfo as a parameter.
/// </summary>
[CreateAssetMenu(fileName = "DataEventVisualFX", menuName = "DataEvents/Tetris/VisualFX")]
public class SO_DataEventVisualFXInfo : SO_DataEventParamBase<VisualFXInfo>
{
}
