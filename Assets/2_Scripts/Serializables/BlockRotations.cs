﻿using System;
using System.Collections.Generic;

/// <summary>
/// Container class that contains a collection of possible rotations for a given block
/// This class existes mainly to allow for e to create a customPropertyDrawer to draw this
/// in a more friendly way in the inspector.
/// </summary>
[Serializable]
public class BlockRotations
{
    public List<BlockCellRotation> rotations;
}
