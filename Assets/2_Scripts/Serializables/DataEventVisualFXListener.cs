﻿using System.Collections.Generic;
using System;

/// <summary>
/// A DataEvent subclass that pass as a Parameter a VisualFXInfo Class.
/// </summary>
[Serializable]
public class DataEventVisualFXListener : DataEventParamListenerBase<VisualFXInfo, SO_DataEventVisualFXInfo>
{
    public List<SO_DataEventVisualFXInfo> dataEvents;

    protected override List<SO_DataEventVisualFXInfo> GetSODataEventList()
    {
        return dataEvents;
    }
}
