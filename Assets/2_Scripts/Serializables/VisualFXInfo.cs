﻿using System;
using System.Collections.Generic;

/// <summary>
/// Container class that contains the cells information that should be passed to the VisualFX so it can animate
/// the lines beeing destroyed with the proper cells colors and positions.
/// </summary>
[Serializable]
public class VisualFXInfo
{
    public List<CellController> cells;
}
