﻿using System;
using System.Collections.Generic;

/// <summary>
/// Container that maintains a list of which positions in the 16 positions List are Active in a given rotation
/// </summary>
[Serializable]
public class BlockCellRotation
{
    public List<int> positions;
}
