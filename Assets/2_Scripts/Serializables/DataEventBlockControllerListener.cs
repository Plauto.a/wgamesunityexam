﻿using System.Collections.Generic;
using System;

/// <summary>
/// A DataEvent subclass that pass as a Parameter a BlockController Monobehaviour.
/// </summary>
[Serializable]
public class DataEventBlockControllerListener : DataEventParamListenerBase<BlockController, SO_DataEventBlockController>
{
    public List<SO_DataEventBlockController> dataEvents;

    protected override List<SO_DataEventBlockController> GetSODataEventList()
    {
        return dataEvents;
    }
}
