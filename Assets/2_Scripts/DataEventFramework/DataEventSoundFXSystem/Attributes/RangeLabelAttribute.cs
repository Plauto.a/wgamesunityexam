﻿using UnityEngine;

public class RangeLabelAttribute : PropertyAttribute
{
    public float Min { get; private set; }
    public float Max { get; private set; }
    public string LeftLabel { get; private set; }
    public string RightLabel { get; private set; }

    public RangeLabelAttribute(float min, float max, string leftLabel, string rightLabel)
    {
        Min = min;
        Max = max;
        LeftLabel = leftLabel;
        RightLabel = rightLabel;
    }

}
