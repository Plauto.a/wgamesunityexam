﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
[Serializable]
public class SO_SoundFX : ScriptableObject {

    [SerializeField]
    private List<AudioClipInfo> audioClips;
    public List<AudioClipInfo> AudioClips { get { return audioClips; } set { audioClips = value; } }

    public void PlayFX(AudioSource audioSource, bool loop = false)
    {
        if (AudioClips.Count > 0)
        {
            if (loop)
            {
                AudioClipInfo clipInfo = AudioClips[UnityEngine.Random.Range(0, AudioClips.Count)];
                audioSource.clip = clipInfo.clip;
                audioSource.pitch = UnityEngine.Random.Range(clipInfo.pitch.minValue, clipInfo.pitch.maxValue);
                audioSource.volume = UnityEngine.Random.Range(clipInfo.volume.minValue, clipInfo.volume.maxValue);
                audioSource.Play();
            }
            else
            {
                AudioClipInfo clipInfo = AudioClips[UnityEngine.Random.Range(0, AudioClips.Count)];
                audioSource.pitch = UnityEngine.Random.Range(clipInfo.pitch.minValue, clipInfo.pitch.maxValue);
                audioSource.PlayOneShot(clipInfo.clip, UnityEngine.Random.Range(clipInfo.volume.minValue, clipInfo.volume.maxValue));
            }
        }
    }

    public void PreviewFXClip(AudioSource audioSource, int clipIndex)
    {
        if (clipIndex < AudioClips.Count)
        {
            AudioClipInfo clipInfo = AudioClips[clipIndex];
            audioSource.pitch = UnityEngine.Random.Range(clipInfo.pitch.minValue, clipInfo.pitch.maxValue);
            audioSource.PlayOneShot(clipInfo.clip, UnityEngine.Random.Range(clipInfo.volume.minValue, clipInfo.volume.maxValue));
        }
    }
}
