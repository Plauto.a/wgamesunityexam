﻿using System;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

[CustomEditor(typeof(SO_SoundFX), true)]
public class SO_SoundFXInspector : Editor {

    [SerializeField] private AudioSource _previewer;

    private ReorderableList reorderableList;
    private float originalLabelWidth;

    public void OnEnable()
    {
        _previewer = EditorUtility.CreateGameObjectWithHideFlags("Audio preview", HideFlags.HideAndDontSave, typeof(AudioSource)).GetComponent<AudioSource>();

        reorderableList = new ReorderableList(serializedObject, serializedObject.FindProperty("audioClips"), true, true, true, true)
        {
            drawElementCallback = DrawElement,
            drawHeaderCallback = (Rect rect) => { EditorGUI.LabelField(rect, "FX Clips"); },
            elementHeight = 54f
        };
    }

    public void OnDisable()
    {
        DestroyImmediate(_previewer.gameObject);
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        reorderableList.DoLayoutList();
        serializedObject.ApplyModifiedProperties();

        EditorGUI.BeginDisabledGroup(serializedObject.isEditingMultipleObjects);
        if (GUILayout.Button("FX Preview"))
        {
            ((SO_SoundFX)target).PlayFX(_previewer);
        }
        EditorGUI.EndDisabledGroup();
    }

    private void DrawElement(Rect rect, int index, bool isActive, bool isFocused)
    {
        var element = reorderableList.serializedProperty.GetArrayElementAtIndex(index);
        rect.y += 2;
        //originalLabelWidth = EditorGUIUtility.labelWidth;
        EditorGUIUtility.labelWidth = 100;
        EditorGUI.PropertyField( new Rect(rect.x, rect.y, rect.width *.75f, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("clip"), new GUIContent("Clip"));
        if (GUI.Button(new Rect(rect.x + rect.width * .75f, rect.y, rect.width * .25f, EditorGUIUtility.singleLineHeight), new GUIContent("Preview")))
        {
            ((SO_SoundFX)target).PreviewFXClip(_previewer, index);
        }
        EditorGUI.PropertyField( new Rect(rect.x, rect.y + 18, rect.width, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("volume"), new GUIContent("Volume"));
        EditorGUI.PropertyField(new Rect(rect.x, rect.y + 36, rect.width, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("pitch"), new GUIContent("Pitch"));
    }
}
