﻿using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

[CustomEditor(typeof(SoundFXPlayer), true)]
public class SoundFXPlayerInspector : Editor
{
    private AudioSource _previewer;

    private ReorderableList reorderableList;

    private SerializedProperty audioSourceProp;
    private SerializedProperty spatialBlendProp;
    private SerializedProperty rolloffModeProp;
    private SerializedProperty minDistanceProp;
    private SerializedProperty maxDistanceProp;

    public void OnEnable()
    {
        _previewer = EditorUtility.CreateGameObjectWithHideFlags("Audio preview", HideFlags.HideAndDontSave, typeof(AudioSource)).GetComponent<AudioSource>();

        reorderableList = new ReorderableList(serializedObject, serializedObject.FindProperty("soundFXDataEventListeners"), true, true, true, true)//soundFxs
        {
            drawElementCallback = DrawElement,
            drawHeaderCallback = (Rect rect) => { EditorGUI.LabelField(rect, "FX Sounds"); },
            elementHeightCallback = ElementHeightCallbackDelegate
        };

        audioSourceProp = serializedObject.FindProperty("audioSource");
        spatialBlendProp = serializedObject.FindProperty("spatialBlend");
        rolloffModeProp = serializedObject.FindProperty("rolloffMode");
        minDistanceProp = serializedObject.FindProperty("minDistance");
        maxDistanceProp = serializedObject.FindProperty("maxDistance");
    }

    public void OnDisable()
    {
        DestroyImmediate(_previewer.gameObject);
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUIUtility.labelWidth = 100;

        reorderableList.DoLayoutList();

        EditorGUILayout.PropertyField(audioSourceProp, new GUIContent("Audio Source"));
        if (audioSourceProp.objectReferenceValue == null)
        {
            EditorGUILayout.Space();
            EditorGUILayout.LabelField(new GUIContent("Auto-ganerated AudioSource Settings"));
            EditorGUILayout.BeginVertical("Box");
            EditorGUILayout.PropertyField(spatialBlendProp, new GUIContent("Spatial Blend"));        
        
            EditorGUILayout.PropertyField(rolloffModeProp, new GUIContent("Volume Rolloff"));
            Rect distanceRect = EditorGUILayout.GetControlRect();
            distanceRect = EditorGUI.PrefixLabel(distanceRect, new GUIContent("Distances"));
            distanceRect.width /= 2;
            EditorGUIUtility.labelWidth = 28;
            EditorGUI.PropertyField(distanceRect, minDistanceProp, new GUIContent("Min"));
            distanceRect.x += distanceRect.width;
            EditorGUI.PropertyField(distanceRect, maxDistanceProp, new GUIContent("Max"));
            GUILayout.EndVertical();
        }
        
        serializedObject.ApplyModifiedProperties();
    }

    private void DrawElement(Rect rect, int index, bool isActive, bool isFocused)
    {
        SerializedProperty element = reorderableList.serializedProperty.GetArrayElementAtIndex(index);
        rect.y += 2;
        SerializedProperty soundEffectProp = element.FindPropertyRelative("soundFX");
        EditorGUI.PropertyField(new Rect(rect.x, rect.y, rect.width * .70f, EditorGUIUtility.singleLineHeight), soundEffectProp, GUIContent.none);
        if (GUI.Button(new Rect(rect.x + rect.width * .74f, rect.y, rect.width * .24f, EditorGUIUtility.singleLineHeight), new GUIContent("Preview")))
        {
            if (soundEffectProp.objectReferenceValue != null)
            {
                if (Application.isPlaying)
                {
                    ((SoundFXPlayer)target).PlayFX(soundEffectProp.objectReferenceValue.name);
                }
                else
                {
                    ((SoundFXPlayer)target).previewFX(soundEffectProp.objectReferenceValue.name, _previewer);
                }
            }
        }

        EditorGUI.PropertyField(new Rect(rect.x, rect.y + 18f, (rect.width * .70f), EditorGUIUtility.singleLineHeight), element, GUIContent.none);
    }

    private float ElementHeightCallbackDelegate(int index)
    {
        SerializedProperty element = reorderableList.serializedProperty.GetArrayElementAtIndex(index);
        //SerializedProperty containersProp = element.FindPropertyRelative("container");

        int arraySize = element.FindPropertyRelative("dataEvents").arraySize;
        if (arraySize > 1)
        {
            return 36 + ((arraySize - 1) * 18);
        }
        return 36;
    }
}
