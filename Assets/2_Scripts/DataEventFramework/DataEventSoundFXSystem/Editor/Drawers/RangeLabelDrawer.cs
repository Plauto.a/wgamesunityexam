﻿using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(RangeLabelAttribute),true)]
public class RangeLabelDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        label = EditorGUI.BeginProperty(position, label, property);

        RangeLabelAttribute rangeLabelAttribute = (RangeLabelAttribute)attribute;

        position = EditorGUI.PrefixLabel(position, label);
        position.height = 16;
        EditorGUI.Slider(position, property, rangeLabelAttribute.Min, rangeLabelAttribute.Max, GUIContent.none);

        position.y += 12;
        position.width -= EditorGUIUtility.labelWidth + 54 - EditorGUIUtility.labelWidth;
        GUIStyle style = EditorStyles.centeredGreyMiniLabel;
        style.alignment = TextAnchor.UpperLeft;
        EditorGUI.LabelField(position, rangeLabelAttribute.LeftLabel, style);

        style.alignment = TextAnchor.UpperRight;
        EditorGUI.LabelField(position, rangeLabelAttribute.RightLabel, style);

        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return 28;
    }
}
