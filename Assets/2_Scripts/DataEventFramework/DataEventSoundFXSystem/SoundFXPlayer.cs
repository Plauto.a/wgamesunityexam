﻿using System.Collections.Generic;
using UnityEngine;

public class SoundFXPlayer : MonoBehaviour {

    [SerializeField]
    private List<SoundFXDataEventListener> soundFXDataEventListeners = new List<SoundFXDataEventListener>();
    public List<SoundFXDataEventListener> SoundFXDataEventListeners { get { return soundFXDataEventListeners; } }
   
    [SerializeField]
    private AudioSource audioSource;
    public AudioSource AudioSource { get { return audioSource; } set { audioSource = value; } }

    [SerializeField]
    private Dictionary<int, AudioSource> loopingAudioSources = new Dictionary<int, AudioSource>();
    public Dictionary<int, AudioSource> LoopingAudioSources { get { return loopingAudioSources; } }

    [SerializeField]
    [RangeLabel(0, 1, "2D", "3D")]
    private float spatialBlend;
    public float SpatialBlend { get { return spatialBlend; } private set { spatialBlend = value; } }

    [SerializeField]
    private SimplifiedRolloffMode rolloffMode;
    public SimplifiedRolloffMode RolloffMode { get { return rolloffMode; } private set { rolloffMode = value; } }

    [SerializeField]
    private float minDistance;
    public float MinDistance { get { return minDistance; } private set { minDistance = value; } }

    [SerializeField]
    private float maxDistance;
    public float MaxDistance { get { return maxDistance; } private set { maxDistance = value; } }

    private void OnEnable()
    {
        if (audioSource == null)
        {
            AudioSource = gameObject.AddComponent<AudioSource>();
            AudioSource.spatialBlend = SpatialBlend;
            AudioSource.rolloffMode = (AudioRolloffMode)RolloffMode;
            AudioSource.minDistance = MinDistance;
            AudioSource.maxDistance = MaxDistance;
        }

        foreach (SoundFXDataEventListener aEventListener in soundFXDataEventListeners)
        {
            aEventListener.Enable(this, audioSource);
        }
    }

    private void OnDisable()
    {
        foreach (SoundFXDataEventListener aEventListener in soundFXDataEventListeners)
        {
            aEventListener.Disable();
        }
    }

    public int PlayFX(string fxName, bool loop = false)
    {
        if (loop)
        {
            SO_SoundFX theSoundFX = FindSoundFX(fxName);
            if (theSoundFX == null)
            {
                return -1;
            }

            //int loopAudioSourceID = Random.Range(1000000, 9999999);
            //AudioSource newLoopAudioSource = CreateNewAudioSource();
            //if (newLoopAudioSource != null)
            //{
            //    loopingAudioSources.Add(loopAudioSourceID, newLoopAudioSource);
            //    newLoopAudioSource.loop = true;
            //    theSoundFX.PlayFX(newLoopAudioSource, true);
            //    return loopAudioSourceID;
            //}
        }
        else
        {
            SO_SoundFX theSoundFX = FindSoundFX(fxName);
            if (theSoundFX != null)
            {
                theSoundFX.PlayFX(AudioSource);
            }
        }
        return -1;
    }

    public void StopLoopingFX(int loopAudioSourceID)
    {
        AudioSource loopingAudioSource = FindLoopingAudioSource(loopAudioSourceID);
        if (loopingAudioSource != null)
        {
            LoopingAudioSources.Remove(loopAudioSourceID);
            Destroy(loopingAudioSource);
        }
    }

    public void previewFX(string fxName, AudioSource previewAudioSource)
    {
        SO_SoundFX theSoundFX = FindSoundFX(fxName);
        if (theSoundFX != null)
        {
            theSoundFX.PlayFX(previewAudioSource);
        }
    }

    //private AudioSource CreateNewAudioSource()
    //{
    //    UnityEditorInternal.ComponentUtility.CopyComponent(audioSource);
    //    UnityEditorInternal.ComponentUtility.PasteComponentAsNew(gameObject);
    //    AudioSource[] audioSources = GetComponents<AudioSource>();
    //    if (audioSources.Length > 0)
    //    {
    //        return audioSources[audioSources.Length - 1];
    //    }
    //    Debug.LogWarning("Fail to create new AudioSource");
    //    return null;
    //}

    private SO_SoundFX FindSoundFX(string fxName)
    {
        foreach (SoundFXDataEventListener aFX in soundFXDataEventListeners)
        {
            if (aFX.soundFX != null && aFX.soundFX.name == fxName)
            {
                return aFX.soundFX;
            }
        }
        Debug.LogWarning("Cannot Located FX Named: " + fxName);
        return null;
    }

    private AudioSource FindLoopingAudioSource(int loopAudioSourceID)
    {
        if (LoopingAudioSources.ContainsKey(loopAudioSourceID))
        {
            return LoopingAudioSources[loopAudioSourceID];
        }
        Debug.LogWarning("Cannot locate looping audiosource with ID: " + loopAudioSourceID);
        return null;
    }
}
