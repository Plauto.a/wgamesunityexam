﻿using System;
using UnityEngine;

[Serializable]
public struct AudioClipInfo {

    public AudioClip clip;
    public RangedFloat volume;
    [MinMaxRange(0, 2)]
    public RangedFloat pitch;

    public AudioClipInfo(AudioClip clip, RangedFloat volume, RangedFloat pitch)
    {
        this.clip = clip;
        this.volume = volume;
        this.pitch = pitch;
    }
}
