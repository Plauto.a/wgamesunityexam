﻿using System;
using UnityEngine;

[Serializable]
public class SoundFXDataEventListener : DataEventListener
{
    public SO_SoundFX soundFX;
    private AudioSource audioSource;
    
    public void Enable(MonoBehaviour mono, AudioSource audioSource)
    {
        this.audioSource = audioSource;
        base.Enable(mono, OnDataEventRaisedHandler);
    }

    public override void Enable(MonoBehaviour mono, Action dataEventHandler)
    {
        Debug.LogError("SoundFXDataEventListener must be Enabled with an audioSource");
    }

    public void OnDataEventRaisedHandler()
    {
        if (soundFX != null && audioSource != null)
        {
            soundFX.PlayFX(audioSource);
        }
    }
}
