﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SO_DataEventInt), true)]
public class SO_DataEventIntInspector: SO_DataEventInspectorBase
{
    private int intDebugValue;

    public override void OnInspectorGUI()
    {
        SO_DataEventInt theDataEvent = (SO_DataEventInt) target;
        intDebugValue = EditorGUILayout.IntField(new GUIContent("Int Debug Value"), intDebugValue);

        BuildRaiseEventButton(theDataEvent, intDebugValue);

        base.OnInspectorGUI();
    }
}