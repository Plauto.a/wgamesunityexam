﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SO_DataEventString), true)]
public class SO_DataEventStringInspector : SO_DataEventInspectorBase
{
    private string stringDebugValue;

    public override void OnInspectorGUI()
    {
        SO_DataEventString theDataEvent = (SO_DataEventString) target;
        stringDebugValue = EditorGUILayout.TextField(new GUIContent("String Debug Value"), stringDebugValue);

        BuildRaiseEventButton(theDataEvent, stringDebugValue);

        base.OnInspectorGUI();
    }
}