﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SO_DataEventVector2), true)]
public class SO_DataEventVector2Inspector: SO_DataEventInspectorBase
{
    private Vector2 vector2DebugValue;

    public override void OnInspectorGUI()
    {
        SO_DataEventVector2 theDataEvent = (SO_DataEventVector2) target;
        vector2DebugValue = EditorGUILayout.Vector2Field(new GUIContent("Vector2 Debug Value"), vector2DebugValue);

        BuildRaiseEventButton(theDataEvent, vector2DebugValue);

        base.OnInspectorGUI();
    }
}