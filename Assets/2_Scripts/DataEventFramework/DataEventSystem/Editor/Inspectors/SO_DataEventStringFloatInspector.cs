﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SO_DataEventStringFloat), true)]
public class SO_DataEventStringFloatInspector : SO_DataEventInspectorBase
{
    private string stringDebugValue;
    private float floatDebugValue;

    public override void OnInspectorGUI()
    {
        SO_DataEventStringFloat theDataEvent = (SO_DataEventStringFloat) target;
        stringDebugValue = EditorGUILayout.TextField(new GUIContent("String Debug Value"), stringDebugValue);
        floatDebugValue = EditorGUILayout.FloatField(new GUIContent("Float Debug Value"), floatDebugValue);

        BuildRaiseEventButton(theDataEvent, stringDebugValue, floatDebugValue);

        base.OnInspectorGUI();
    }
}