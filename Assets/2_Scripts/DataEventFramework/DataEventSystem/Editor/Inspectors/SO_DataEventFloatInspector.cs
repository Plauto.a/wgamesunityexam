﻿using UnityEditor;
using System.Collections.Generic;
using UnityEngine;

[CustomEditor(typeof(SO_DataEventFloat), true)]
public class SO_DataEventFloatInspector: SO_DataEventInspectorBase
{
    private float floatDebugValue;

    public override void OnInspectorGUI()
    {
        SO_DataEventFloat theDataEvent = (SO_DataEventFloat) target;
        floatDebugValue = EditorGUILayout.FloatField(new GUIContent("Float Debug Value"), floatDebugValue);

        BuildRaiseEventButton(theDataEvent, floatDebugValue);

        base.OnInspectorGUI();
    }
}