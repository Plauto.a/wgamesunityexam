﻿using UnityEditor;
using System.Collections.Generic;
using UnityEngine;

[CustomEditor(typeof(SO_DataEvent), true)]
public class SO_DataEventInspector : SO_DataEventInspectorBase
{
    public override void OnInspectorGUI()
    {
        SO_DataEvent theDataEvent = (SO_DataEvent) target;

        if (!Application.isPlaying)
        {
            GUI.enabled = false;
        }
        if (GUILayout.Button("Raise Data Event"))
        {
            theDataEvent.Raise();
        }
        GUI.enabled = true;

        base.OnInspectorGUI();
    }
}