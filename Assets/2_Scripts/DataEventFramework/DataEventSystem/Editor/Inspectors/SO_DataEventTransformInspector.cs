﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SO_DataEventTransform), true)]
public class SO_DataEventTransformInspector : SO_DataEventInspectorBase
{
    private Transform transformDebugValue;

    public override void OnInspectorGUI()
    {
        SO_DataEventTransform theDataEvent = (SO_DataEventTransform) target;
        transformDebugValue = (Transform)EditorGUILayout.ObjectField(new GUIContent("Transform Debug Value"), transformDebugValue, typeof(Transform), true);

        BuildRaiseEventButton(theDataEvent, transformDebugValue);

        base.OnInspectorGUI();
    }
}