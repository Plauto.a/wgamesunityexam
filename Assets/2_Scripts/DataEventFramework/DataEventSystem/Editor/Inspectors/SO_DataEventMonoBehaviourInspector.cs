﻿using UnityEditor;
using System.Collections.Generic;
using UnityEngine;

[CustomEditor(typeof(SO_DataEventMonoBehaviour), true)]
public class SO_DataEventMonoBehaviourInspector : SO_DataEventInspectorBase
{
    private MonoBehaviour monoBehaviourDebugValue;

    public override void OnInspectorGUI()
    {
        SO_DataEventMonoBehaviour theDataEvent = (SO_DataEventMonoBehaviour) target;
        monoBehaviourDebugValue = (MonoBehaviour)EditorGUILayout.ObjectField(new GUIContent("MonoBehaviour Debug Value"), monoBehaviourDebugValue,typeof(MonoBehaviour), true);

        BuildRaiseEventButton(theDataEvent, monoBehaviourDebugValue);

        base.OnInspectorGUI();
    }
}