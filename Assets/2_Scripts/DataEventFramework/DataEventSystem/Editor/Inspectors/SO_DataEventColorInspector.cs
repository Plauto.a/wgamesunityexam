﻿using UnityEditor;
using System.Collections.Generic;
using UnityEngine;

[CustomEditor(typeof(SO_DataEventColor), true)]
public class SO_DataEventColorInspector: SO_DataEventInspectorBase
{
    private Color colorDebugValue;

    public override void OnInspectorGUI()
    {
        SO_DataEventColor theDataEvent = (SO_DataEventColor) target;
        colorDebugValue = EditorGUILayout.ColorField(new GUIContent("Color Debug Value"), colorDebugValue);

        BuildRaiseEventButton(theDataEvent, colorDebugValue);

        base.OnInspectorGUI();
    }
}