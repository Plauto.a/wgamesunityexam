﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SO_DataEventVector3), true)]
public class SO_DataEventVector3Inspector: SO_DataEventInspectorBase
{
    private Vector3 vector3DebugValue;

    public override void OnInspectorGUI()
    {
        SO_DataEventVector3 theDataEvent = (SO_DataEventVector3) target;
        vector3DebugValue = EditorGUILayout.Vector3Field(new GUIContent("Vector3 Debug Value"), vector3DebugValue);

        BuildRaiseEventButton(theDataEvent, vector3DebugValue);

        base.OnInspectorGUI();
    }
}