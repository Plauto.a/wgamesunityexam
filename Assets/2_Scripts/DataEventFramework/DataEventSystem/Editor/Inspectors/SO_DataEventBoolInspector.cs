﻿using UnityEditor;
using System.Collections.Generic;
using UnityEngine;

[CustomEditor(typeof(SO_DataEventBool), true)]
public class SO_DataEventBoolInspector: SO_DataEventInspectorBase
{
    private bool boolDebugValue;

    public override void OnInspectorGUI()
    {
        SO_DataEventBool theDataEvent = (SO_DataEventBool) target;
        boolDebugValue = EditorGUILayout.Toggle(new GUIContent("Bool Debug Value"), boolDebugValue);

        BuildRaiseEventButton(theDataEvent, boolDebugValue);

        base.OnInspectorGUI();
    }
}