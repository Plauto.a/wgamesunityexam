﻿using UnityEditor;
using System.Collections.Generic;
using UnityEngine;

[CustomEditor(typeof(SO_DataEventBase), true)]
public class SO_DataEventInspectorBase : Editor
{
    public override void OnInspectorGUI()
    {
        SO_DataEventBase theDataEvent = (SO_DataEventBase)target;

        List<string> listenersIds = theDataEvent.RegisteredListenersIds();
        if (listenersIds.Count > 0)
        {
            EditorGUILayout.LabelField(new GUIContent("Current Listeners"));
            EditorGUILayout.BeginVertical("Box");
            for (int i = 0; i < listenersIds.Count; i++)
            {
                EditorGUILayout.LabelField(new GUIContent(listenersIds[i]));
            }
            EditorGUILayout.EndVertical();
        }
    }

    protected void BuildRaiseEventButton<T1, T2>(T1 dataEvent, T2 param) where T1: SO_DataEventParamBase<T2>
    {
        if (!Application.isPlaying)
        {
            GUI.enabled = false;
        }
        if (GUILayout.Button("Raise Data Event"))
        {
            dataEvent.Raise(param);
        }
        GUI.enabled = true;
    }

    protected void BuildRaiseEventButton<T1, T2, T3>(T1 dataEvent, T2 param, T3 param2) where T1 : SO_DataEventTwoParamBase<T2, T3>
    {
        if (!Application.isPlaying)
        {
            GUI.enabled = false;
        }
        if (GUILayout.Button("Raise Data Event"))
        {
            dataEvent.Raise(param, param2);
        }
        GUI.enabled = true;
    }
}