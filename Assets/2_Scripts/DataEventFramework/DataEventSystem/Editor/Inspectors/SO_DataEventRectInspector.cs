﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SO_DataEventRect), true)]
public class SO_DataEventRectInspector: SO_DataEventInspectorBase
{
    private Rect rectDebugValue;

    public override void OnInspectorGUI()
    {
        SO_DataEventRect theDataEvent = (SO_DataEventRect) target;
        rectDebugValue = EditorGUILayout.RectField(new GUIContent("Rect Debug Value"), rectDebugValue);

        BuildRaiseEventButton(theDataEvent, rectDebugValue);

        base.OnInspectorGUI();
    }
}