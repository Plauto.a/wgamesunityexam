﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SO_DataEventSprite), true)]
public class SO_DataEventSpriteInspector: SO_DataEventInspectorBase
{
    private Sprite spriteDebugValue;

    public override void OnInspectorGUI()
    {
        SO_DataEventSprite theDataEvent = (SO_DataEventSprite) target;
        spriteDebugValue = (Sprite)EditorGUILayout.ObjectField(new GUIContent("Sprite Debug Value"), spriteDebugValue, typeof(Sprite), true);

        BuildRaiseEventButton(theDataEvent, spriteDebugValue);

        base.OnInspectorGUI();
    }
}