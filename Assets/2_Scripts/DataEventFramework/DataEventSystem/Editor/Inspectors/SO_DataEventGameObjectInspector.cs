﻿using UnityEditor;
using System.Collections.Generic;
using UnityEngine;

[CustomEditor(typeof(SO_DataEventGameObject), true)]
public class SO_DataEventGameObjectInspector: SO_DataEventInspectorBase
{
    private GameObject gameObjectDebugValue;

    public override void OnInspectorGUI()
    {
        SO_DataEventGameObject theDataEvent = (SO_DataEventGameObject) target;
        gameObjectDebugValue = (GameObject)EditorGUILayout.ObjectField(new GUIContent("GameObject Debug Value"), gameObjectDebugValue,typeof(GameObject), true);

        BuildRaiseEventButton(theDataEvent, gameObjectDebugValue);

        base.OnInspectorGUI();
    }
}