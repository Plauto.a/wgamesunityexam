﻿using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(DataEventListener), true)]
[CustomPropertyDrawer(typeof(DataEventBoolListener), true)]
[CustomPropertyDrawer(typeof(DataEventColorListener), true)]
[CustomPropertyDrawer(typeof(DataEventFloatListener), true)]
[CustomPropertyDrawer(typeof(DataEventGameObjectListener), true)]
[CustomPropertyDrawer(typeof(DataEventIntListener), true)]
[CustomPropertyDrawer(typeof(DataEventMonoBehaviourListener), true)]
[CustomPropertyDrawer(typeof(DataEventRectListener), true)]
[CustomPropertyDrawer(typeof(DataEventSpriteListener), true)]
[CustomPropertyDrawer(typeof(DataEventStringListener), true)]
[CustomPropertyDrawer(typeof(DataEventStringFloatListener), true)]
[CustomPropertyDrawer(typeof(DataEventTransformListener), true)]
[CustomPropertyDrawer(typeof(DataEventVector2Listener), true)]
[CustomPropertyDrawer(typeof(DataEventVector3Listener), true)]
[CustomPropertyDrawer(typeof(DataEventBlockControllerListener), true)]
[CustomPropertyDrawer(typeof(DataEventVisualFXListener), true)]
public class GenericDataEventListenerDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        SerializedProperty dataEventsProp = property.FindPropertyRelative("dataEvents");
        if (dataEventsProp.arraySize > 0)
        {
            float lineWidth = dataEventsProp.arraySize > 1 ? position.width : position.width - 18;
            Rect boxSize = new Rect(position.x, position.y, lineWidth, dataEventsProp.arraySize * 18);

            EditorGUI.HelpBox(boxSize, "", MessageType.None);
            for (int i = 0; i < dataEventsProp.arraySize; i++)
            {
                SerializedProperty aEventProp = dataEventsProp.GetArrayElementAtIndex(i);

                EditorGUI.PropertyField(new Rect(position.x, position.y, position.width - 36, EditorGUIUtility.singleLineHeight), aEventProp, label);

                if (GUI.Button(new Rect(position.x + position.width - 36, position.y, 18, EditorGUIUtility.singleLineHeight), new GUIContent("+")))
                {
                    dataEventsProp.InsertArrayElementAtIndex(dataEventsProp.arraySize);
                }
                if (dataEventsProp.arraySize > 1)
                {
                    if (GUI.Button(new Rect(position.x + position.width - 18, position.y, 18, EditorGUIUtility.singleLineHeight), new GUIContent("-")))
                    {
                        dataEventsProp.DeleteArrayElementAtIndex(i);
                    }
                }
                position.y += 18;
            }
        }
        else
        {
            dataEventsProp.InsertArrayElementAtIndex(dataEventsProp.arraySize);
        }
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        SerializedProperty dataEventsProp = property.FindPropertyRelative("dataEvents");

        if (dataEventsProp.arraySize > 0)
        {
            return dataEventsProp.arraySize * 18;
        }
        else
        {
            return 16;
        }

    }
}
