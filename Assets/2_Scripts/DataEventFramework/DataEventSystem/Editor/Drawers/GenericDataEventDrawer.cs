﻿using UnityEngine;
using UnityEditor;
/*
--possible Icons--
CollabPull
CollabPush
SceneLoadIn
SceneLoadOut
TimelineContinue
*/
[CustomPropertyDrawer(typeof(SO_DataEvent), true)]
[CustomPropertyDrawer(typeof(SO_DataEventBool), true)]
[CustomPropertyDrawer(typeof(SO_DataEventColor), true)]
[CustomPropertyDrawer(typeof(SO_DataEventFloat), true)]
[CustomPropertyDrawer(typeof(SO_DataEventGameObject), true)]
[CustomPropertyDrawer(typeof(SO_DataEventInt), true)]
[CustomPropertyDrawer(typeof(SO_DataEventMonoBehaviour), true)]
[CustomPropertyDrawer(typeof(SO_DataEventRect), true)]
[CustomPropertyDrawer(typeof(SO_DataEventSprite), true)]
[CustomPropertyDrawer(typeof(SO_DataEventString), true)]
[CustomPropertyDrawer(typeof(SO_DataEventStringFloat), true)]
[CustomPropertyDrawer(typeof(SO_DataEventTransform), true)]
[CustomPropertyDrawer(typeof(SO_DataEventVector2), true)]
[CustomPropertyDrawer(typeof(SO_DataEventVector3), true)]
[CustomPropertyDrawer(typeof(SO_DataEventBlockController), true)]
[CustomPropertyDrawer(typeof(SO_DataEventVisualFXInfo), true)]
public class GenericDataEventDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        GUIContent eyeIcon = EditorGUIUtility.IconContent("ViewToolOrbit");
        EditorGUI.LabelField(new Rect(position.x, position.y, 22, 22), eyeIcon);

        EditorGUI.PropertyField(new Rect(position.x + 22, position.y+1, position.width - 22, EditorGUIUtility.singleLineHeight), property, label);
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return 18;
    }
}

