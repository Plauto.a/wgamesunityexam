﻿using System;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class UnityEventBool : UnityEvent<bool>{}
[Serializable]
public class UnityEventColor : UnityEvent<Color> { }
[Serializable]
public class UnityEventFloat : UnityEvent<float> { }
[Serializable]
public class UnityEventGameObject : UnityEvent<GameObject> { }
[Serializable]
public class UnityEventInt : UnityEvent<int> { }
[Serializable]
public class UnityEventMonoBehaviour : UnityEvent<MonoBehaviour> { }
[Serializable]
public class UnityEventRect : UnityEvent<Rect> { }
[Serializable]
public class UnityEventSprite : UnityEvent<Sprite> { }
[Serializable]
public class UnityEventString : UnityEvent<string> { }
[Serializable]
public class UnityEventTransform : UnityEvent<Transform> { }
[Serializable]
public class UnityEventVector2 : UnityEvent<Vector2> { }
[Serializable]
public class UnityEventVector3 : UnityEvent<Vector3> { }
