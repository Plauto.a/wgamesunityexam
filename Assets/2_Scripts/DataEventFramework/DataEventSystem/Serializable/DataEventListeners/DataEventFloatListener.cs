﻿using System;
using System.Collections.Generic;

[Serializable]
public class DataEventFloatListener : DataEventParamListenerBase<float, SO_DataEventFloat>
{
    public List<SO_DataEventFloat> dataEvents;

    protected override List<SO_DataEventFloat> GetSODataEventList()
    {
        return dataEvents;
    }
}
