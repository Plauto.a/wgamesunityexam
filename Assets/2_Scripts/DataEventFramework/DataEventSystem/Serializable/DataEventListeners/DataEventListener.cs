﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DataEventListener : IDataEventListener
{
    public List<SO_DataEvent> dataEvents;
    public Action DataEventHandler = delegate { };

    private string id;

    public string GetListenerId() { return id; }

    public void OnDataEventRaised()
    {
        DataEventHandler();
    }

    public virtual void Enable(MonoBehaviour mono, Action dataEventHandler)
    {
        if (dataEvents != null && dataEvents.Count > 0)
        {
            DataEventHandler = dataEventHandler;
            id = mono.GetPath();
            for (int i = dataEvents.Count - 1; i >= 0; i--)
            {
                if (dataEvents[i] != null)
                {
                    dataEvents[i].RegisterListener(this);
                }
            }
        }
    }

    public virtual void Disable()
    {
        if (dataEvents != null && dataEvents.Count > 0)
        {
            for (int i = dataEvents.Count - 1; i >= 0; i--)
            {
                if (dataEvents[i] != null)
                {
                    dataEvents[i].UnregisterListener(this);
                }
            }
        }
    }
}
