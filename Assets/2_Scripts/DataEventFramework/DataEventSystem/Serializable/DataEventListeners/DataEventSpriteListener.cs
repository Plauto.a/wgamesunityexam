﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DataEventSpriteListener : DataEventParamListenerBase<Sprite, SO_DataEventSprite>
{
    public List<SO_DataEventSprite> dataEvents;

    protected override List<SO_DataEventSprite> GetSODataEventList()
    {
        return dataEvents;
    }
}
