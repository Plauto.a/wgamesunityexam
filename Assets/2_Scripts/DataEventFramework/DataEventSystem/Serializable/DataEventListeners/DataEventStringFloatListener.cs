﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DataEventStringFloatListener : DataEventTwoParamListenerBase<string, float, SO_DataEventStringFloat>
{
    public List<SO_DataEventStringFloat> dataEvents;

    protected override List<SO_DataEventStringFloat> GetSODataEventList()
    {
        return dataEvents;
    }
}
