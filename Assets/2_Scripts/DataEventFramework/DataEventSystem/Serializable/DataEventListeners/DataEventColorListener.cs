﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DataEventColorListener : DataEventParamListenerBase<Color, SO_DataEventColor>
{
    public List<SO_DataEventColor> dataEvents;

    protected override List<SO_DataEventColor> GetSODataEventList()
    {
        return dataEvents;
    }
}
