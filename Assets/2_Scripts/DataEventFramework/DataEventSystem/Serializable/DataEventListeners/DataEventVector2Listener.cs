﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DataEventVector2Listener : DataEventParamListenerBase<Vector2, SO_DataEventVector2>
{
    public List<SO_DataEventVector2> dataEvents;

    protected override List<SO_DataEventVector2> GetSODataEventList()
    {
        return dataEvents;
    }
}
