﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DataEventGameObjectListener : DataEventParamListenerBase<GameObject, SO_DataEventGameObject>
{
    public List<SO_DataEventGameObject> dataEvents;

    protected override List<SO_DataEventGameObject> GetSODataEventList()
    {
        return dataEvents;
    }
}
