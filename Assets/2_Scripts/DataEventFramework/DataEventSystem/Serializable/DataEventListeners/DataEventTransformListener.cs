﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DataEventTransformListener : DataEventParamListenerBase<Transform, SO_DataEventTransform>
{
    public List<SO_DataEventTransform> dataEvents;

    protected override List<SO_DataEventTransform> GetSODataEventList()
    {
        return dataEvents;
    }
}
