﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DataEventMonoBehaviourListener : DataEventParamListenerBase<MonoBehaviour, SO_DataEventMonoBehaviour>
{
    public List<SO_DataEventMonoBehaviour> dataEvents;

    protected override List<SO_DataEventMonoBehaviour> GetSODataEventList()
    {
        return dataEvents;
    }
}
