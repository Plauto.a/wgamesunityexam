﻿using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class DataEventParamListenerBase<T1, T3> : IDataEventParamListener<T1> where T3 : SO_DataEventParamBase<T1>
{
    public Action<T1> DataEventHandler = delegate { };

    private string id;
    public string GetListenerId() { return id; }

    public void OnDataEventRaised(T1 param)
    {
        DataEventHandler(param);
    }

    protected abstract List<T3> GetSODataEventList();

    public virtual void Enable(MonoBehaviour mono, Action<T1> dataEventHandler)
    {
        List<T3> dataEvents = GetSODataEventList();
        if (dataEvents != null && dataEvents.Count > 0)
        {
            DataEventHandler = dataEventHandler;
            id = mono.GetPath();
            for (int i = dataEvents.Count - 1; i >= 0; i--)
            {
                if (dataEvents[i] != null)
                {
                    dataEvents[i].RegisterListener(this);
                }
            }
        }
    }
    public virtual void Disable()
    {
        List<T3> dataEvents = GetSODataEventList();
        if (dataEvents != null && dataEvents.Count > 0)
        {
            for (int i = dataEvents.Count - 1; i >= 0; i--)
            {
                if (dataEvents[i] != null)
                {
                    dataEvents[i].UnregisterListener(this);
                }
            }
        }
    }
}
