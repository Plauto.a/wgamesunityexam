﻿using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class DataEventTwoParamListenerBase<T1, T2, T3> : IDataEventTwoParamListener<T1, T2> where T3 : SO_DataEventTwoParamBase<T1, T2>
{
    public Action<T1, T2> DataEventHandler = delegate { };

    private string id;
    public string GetListenerId() { return id; }

    public void OnDataEventRaised(T1 param, T2 param2)
    {
        DataEventHandler(param, param2);
    }

    protected abstract List<T3> GetSODataEventList();

    public virtual void Enable(MonoBehaviour mono, Action<T1, T2> dataEventHandler)
    {
        List<T3> dataEvents = GetSODataEventList();
        if (dataEvents != null && dataEvents.Count > 0)
        {
            DataEventHandler = dataEventHandler;
            id = mono.GetPath();
            for (int i = dataEvents.Count - 1; i >= 0; i--)
            {
                if (dataEvents[i] != null)
                {
                    dataEvents[i].RegisterListener(this);
                }
            }
        }
    }
    public virtual void Disable()
    {
        List<T3> dataEvents = GetSODataEventList();
        if (dataEvents != null && dataEvents.Count > 0)
        {
            for (int i = dataEvents.Count - 1; i >= 0; i--)
            {
                if (dataEvents[i] != null)
                {
                    dataEvents[i].UnregisterListener(this);
                }
            }
        }
    }
}
