﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DataEventStringListener : DataEventParamListenerBase<string, SO_DataEventString>
{
    public List<SO_DataEventString> dataEvents;

    protected override List<SO_DataEventString> GetSODataEventList()
    {
        return dataEvents;
    }
}
