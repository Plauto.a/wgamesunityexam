﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DataEventIntListener : DataEventParamListenerBase<int, SO_DataEventInt>
{
    public List<SO_DataEventInt> dataEvents;

    protected override List<SO_DataEventInt> GetSODataEventList()
    {
        return dataEvents;
    }
}
