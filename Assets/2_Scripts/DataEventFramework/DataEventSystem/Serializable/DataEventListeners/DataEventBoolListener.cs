﻿using System;
using System.Collections.Generic;

[Serializable]
public class DataEventBoolListener : DataEventParamListenerBase<bool, SO_DataEventBool>
{
    public List<SO_DataEventBool> dataEvents;

    protected override List<SO_DataEventBool> GetSODataEventList()
    {
        return dataEvents;
    }
}
