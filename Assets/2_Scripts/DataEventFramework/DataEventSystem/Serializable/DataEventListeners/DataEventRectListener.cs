﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DataEventRectListener : DataEventParamListenerBase<Rect, SO_DataEventRect>
{
    public List<SO_DataEventRect> dataEvents;

    protected override List<SO_DataEventRect> GetSODataEventList()
    {
        return dataEvents;
    }
}
