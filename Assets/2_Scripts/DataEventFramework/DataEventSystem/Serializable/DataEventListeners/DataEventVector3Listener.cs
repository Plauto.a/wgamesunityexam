﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DataEventVector3Listener : DataEventParamListenerBase<Vector3, SO_DataEventVector3>
{
    public List<SO_DataEventVector3> dataEvents;

    protected override List<SO_DataEventVector3> GetSODataEventList()
    {
        return dataEvents;
    }
}
