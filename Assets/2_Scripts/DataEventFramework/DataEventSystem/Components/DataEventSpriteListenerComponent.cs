﻿using UnityEngine;

public class DataEventSpriteListenerComponent : MonoBehaviour
{
    [SerializeField]
    private DataEventSpriteListener evnt;

    [SerializeField]
    private UnityEventSprite unityEvent;

    private void OnEnable()
    {
        evnt.Enable(this, DataEventHandler);
    }

    private void OnDisable()
    {
        evnt.Disable();
    }

    private void DataEventHandler(Sprite param)
    {
        unityEvent.Invoke(param);
    }
}


