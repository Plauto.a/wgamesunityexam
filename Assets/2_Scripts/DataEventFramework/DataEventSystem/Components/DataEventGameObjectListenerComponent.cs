﻿using UnityEngine;

public class DataEventGameObjectListenerComponent : MonoBehaviour
{
    [SerializeField]
    private DataEventGameObjectListener evnt;

    [SerializeField]
    private UnityEventGameObject unityEvent;

    private void OnEnable()
    {
        evnt.Enable(this, DataEventHandler);
    }

    private void OnDisable()
    {
        evnt.Disable();
    }

    private void DataEventHandler(GameObject param)
    {
        unityEvent.Invoke(param);
    }
}


