﻿using UnityEngine;

public class DataEventColorListenerComponent : MonoBehaviour
{
    [SerializeField]
    private DataEventColorListener evnt;

    [SerializeField]
    private UnityEventColor unityEvent;

    private void OnEnable()
    {
        evnt.Enable(this, DataEventHandler);
    }

    private void OnDisable()
    {
        evnt.Disable();
    }

    private void DataEventHandler(Color param)
    {
        unityEvent.Invoke(param);
    }
}


