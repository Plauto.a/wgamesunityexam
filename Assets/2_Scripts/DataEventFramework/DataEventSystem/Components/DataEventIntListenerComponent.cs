﻿using UnityEngine;

public class DataEventIntListenerComponent : MonoBehaviour
{
    [SerializeField]
    private DataEventIntListener evnt;

    [SerializeField]
    private UnityEventInt unityEvent;

    private void OnEnable()
    {
        evnt.Enable(this, DataEventHandler);
    }

    private void OnDisable()
    {
        evnt.Disable();
    }

    private void DataEventHandler(int param)
    {
        unityEvent.Invoke(param);
    }
}


