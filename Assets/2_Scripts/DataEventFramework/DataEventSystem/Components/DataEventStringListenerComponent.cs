﻿using UnityEngine;

public class DataEventStringListenerComponent : MonoBehaviour
{
    [SerializeField]
    private DataEventStringListener evnt;

    [SerializeField]
    private UnityEventString unityEvent;

    private void OnEnable()
    {
        evnt.Enable(this, DataEventHandler);
    }

    private void OnDisable()
    {
        evnt.Disable();
    }

    private void DataEventHandler(string param)
    {
        unityEvent.Invoke(param);
    }
}


