﻿using UnityEngine;

public class DataEventRectListenerComponent : MonoBehaviour
{
    [SerializeField]
    private DataEventRectListener evnt;

    [SerializeField]
    private UnityEventRect unityEvent;

    private void OnEnable()
    {
        evnt.Enable(this, DataEventHandler);
    }

    private void OnDisable()
    {
        evnt.Disable();
    }

    private void DataEventHandler(Rect param)
    {
        unityEvent.Invoke(param);
    }
}


