﻿using UnityEngine;

public class DataEventMonoBehaviourComponent : MonoBehaviour
{
    [SerializeField]
    private DataEventMonoBehaviourListener evnt;

    [SerializeField]
    private UnityEventMonoBehaviour unityEvent;

    private void OnEnable()
    {
        evnt.Enable(this, DataEventHandler);
    }

    private void OnDisable()
    {
        evnt.Disable();
    }

    private void DataEventHandler(MonoBehaviour param)
    {
        unityEvent.Invoke(param);
    }
}


