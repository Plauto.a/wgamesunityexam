﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DataEventListenerComponent : MonoBehaviour {

    [SerializeField]
    private DataEventListener evnt;

    [SerializeField]
    private UnityEvent unityEvent;

    private void OnEnable()
    {
        evnt.Enable(this, DataEventHandler);
    }

    private void OnDisable()
    {
        evnt.Disable();
    }

    private void DataEventHandler()
    {
        unityEvent.Invoke();
    }
}
