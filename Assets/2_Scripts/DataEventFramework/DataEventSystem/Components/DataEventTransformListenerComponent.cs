﻿using UnityEngine;

public class DataEventTransformListenerComponent : MonoBehaviour
{
    [SerializeField]
    private DataEventTransformListener evnt;

    [SerializeField]
    private UnityEventTransform unityEvent;

    private void OnEnable()
    {
        evnt.Enable(this, DataEventHandler);
    }

    private void OnDisable()
    {
        evnt.Disable();
    }

    private void DataEventHandler(Transform param)
    {
        unityEvent.Invoke(param);
    }
}


