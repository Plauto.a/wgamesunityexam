﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DataEventVector2ListenerComponent : MonoBehaviour
{
    [SerializeField]
    private DataEventVector2Listener evnt;

    [SerializeField]
    private UnityEventVector2 unityEvent;

    private void OnEnable()
    {
        evnt.Enable(this, DataEventHandler);
    }

    private void OnDisable()
    {
        evnt.Disable();
    }

    private void DataEventHandler(Vector2 param)
    {
        unityEvent.Invoke(param);
    }
}


