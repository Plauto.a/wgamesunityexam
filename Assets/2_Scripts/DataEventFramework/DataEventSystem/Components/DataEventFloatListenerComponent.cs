﻿using UnityEngine;

public class DataEventFloatListenerComponent : MonoBehaviour
{
    [SerializeField]
    private DataEventFloatListener evnt;

    [SerializeField]
    private UnityEventFloat unityEvent;

    private void OnEnable()
    {
        evnt.Enable(this, DataEventHandler);
    }

    private void OnDisable()
    {
        evnt.Disable();
    }

    private void DataEventHandler(float param)
    {
        unityEvent.Invoke(param);
    }
}


