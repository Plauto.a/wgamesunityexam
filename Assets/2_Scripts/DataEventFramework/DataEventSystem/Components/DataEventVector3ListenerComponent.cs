﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DataEventVector3ListenerComponent : MonoBehaviour
{
    [SerializeField]
    private DataEventVector3Listener evnt;

    [SerializeField]
    private UnityEventVector3 unityEvent;

    private void OnEnable()
    {
        evnt.Enable(this, DataEventHandler);
    }

    private void OnDisable()
    {
        evnt.Disable();
    }

    private void DataEventHandler(Vector3 param)
    {
        unityEvent.Invoke(param);
    }
}


