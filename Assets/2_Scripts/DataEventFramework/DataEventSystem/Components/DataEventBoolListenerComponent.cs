﻿using UnityEngine;

public class DataEventBoolListenerComponent : MonoBehaviour
{
    [SerializeField]
    private DataEventBoolListener evnt;

    [SerializeField]
    private UnityEventBool unityEvent;

    private void OnEnable()
    {
        evnt.Enable(this, DataEventHandler);
        //evnt.DataEventHandler = DataEventHandler;
    }

    private void OnDisable()
    {
        evnt.Disable();
    }

    private void DataEventHandler(bool param)
    {
        unityEvent.Invoke(param);
    }
}


