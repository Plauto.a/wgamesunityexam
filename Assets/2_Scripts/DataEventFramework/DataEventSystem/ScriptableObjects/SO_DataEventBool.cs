﻿using UnityEngine;

[CreateAssetMenu(fileName = "DataEventBool", menuName = "DataEvents/Bool")]
public class SO_DataEventBool : SO_DataEventParamBase<bool> {}
