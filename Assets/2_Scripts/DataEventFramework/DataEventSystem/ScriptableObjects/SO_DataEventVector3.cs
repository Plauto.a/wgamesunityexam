﻿using UnityEngine;

[CreateAssetMenu(fileName = "DataEventVector3", menuName = "DataEvents/Vector3")]
public class SO_DataEventVector3 : SO_DataEventParamBase<Vector3> {}
