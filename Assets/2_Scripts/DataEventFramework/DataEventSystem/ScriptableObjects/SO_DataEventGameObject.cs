﻿using UnityEngine;

[CreateAssetMenu(fileName = "DataEventGameObject", menuName = "DataEvents/GameObject")]
public class SO_DataEventGameObject : SO_DataEventParamBase<GameObject> {}
