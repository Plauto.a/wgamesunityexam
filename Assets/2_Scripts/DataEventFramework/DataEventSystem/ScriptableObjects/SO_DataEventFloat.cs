﻿using UnityEngine;

[CreateAssetMenu(fileName = "DataEventFloat", menuName = "DataEvents/Float")]
public class SO_DataEventFloat : SO_DataEventParamBase<float> {}
