﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DataEvent", menuName = "DataEvents/No Parameter")]
public class SO_DataEvent : SO_DataEventBase {

    [SerializeField]
    private List<IDataEventListener> listeners = new List<IDataEventListener>();
    
    public void Raise()
    {
        for (int i = listeners.Count -1; i >= 0 ; i--)
        {
            listeners[i].OnDataEventRaised();
        }
    }

    public void RegisterListener(IDataEventListener listener)
    {
        if (!listeners.Contains(listener))
        {
            listeners.Add(listener);
        }
    }

    public void UnregisterListener(IDataEventListener listener)
    {
        if (listeners.Contains(listener))
        {
            listeners.Remove(listener);
        }
    }

    public override List<string> RegisteredListenersIds()
    {
        List<string> listenersIds = new List<string>();
        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            listenersIds.Add(listeners[i].GetListenerId());
        }
        return listenersIds;
    }
}
