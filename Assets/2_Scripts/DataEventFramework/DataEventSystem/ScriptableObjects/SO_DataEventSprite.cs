﻿using UnityEngine;

[CreateAssetMenu(fileName = "DataEventSprite", menuName = "DataEvents/Sprite")]
public class SO_DataEventSprite : SO_DataEventParamBase<Sprite> {}
