﻿using UnityEngine;

[CreateAssetMenu(fileName = "DataEventTransform", menuName = "DataEvents/Transform")]
public class SO_DataEventTransform : SO_DataEventParamBase<Transform> {}
