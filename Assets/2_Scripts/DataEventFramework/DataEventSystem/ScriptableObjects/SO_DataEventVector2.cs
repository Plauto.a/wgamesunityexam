﻿using UnityEngine;

[CreateAssetMenu(fileName = "DataEventVector2", menuName = "DataEvents/Vector2")]
public class SO_DataEventVector2 : SO_DataEventParamBase<Vector2> {}
