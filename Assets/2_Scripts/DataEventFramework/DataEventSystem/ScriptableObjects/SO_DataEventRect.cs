﻿using UnityEngine;

[CreateAssetMenu(fileName = "DataEventRect", menuName = "DataEvents/Rect")]
public class SO_DataEventRect : SO_DataEventParamBase<Rect> {}
