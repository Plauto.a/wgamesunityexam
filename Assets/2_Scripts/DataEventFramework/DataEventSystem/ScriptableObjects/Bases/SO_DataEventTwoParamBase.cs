﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SO_DataEventTwoParamBase<T1, T2> : SO_DataEventBase
{
    [SerializeField]
    private List<IDataEventTwoParamListener<T1, T2>> listeners = new List<IDataEventTwoParamListener<T1, T2>>();

    public void Raise(T1 param, T2 param2)
    {
        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            listeners[i].OnDataEventRaised(param, param2);
        }
    }

    public void RegisterListener(IDataEventTwoParamListener<T1, T2> listener)
    {
        if (!listeners.Contains(listener))
        {
            listeners.Add(listener);
        }
    }

    public void UnregisterListener(IDataEventTwoParamListener<T1, T2> listener)
    {
        if (listeners.Contains(listener))
        {
            listeners.Remove(listener);
        }
    }

    public override List<string> RegisteredListenersIds()
    {
        List<string> listenersIds = new List<string>();
        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            listenersIds.Add(listeners[i].GetListenerId());
        }
        return listenersIds;
    }
}