﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SO_DataEventBase : ScriptableObject {
    public abstract List<string> RegisteredListenersIds();
}
