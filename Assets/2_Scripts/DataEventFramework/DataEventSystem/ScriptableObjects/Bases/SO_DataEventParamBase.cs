﻿using System.Collections.Generic;
using UnityEngine;

public abstract class SO_DataEventParamBase<T> : SO_DataEventBase
{
    [SerializeField]
    private List<IDataEventParamListener<T>> listeners = new List<IDataEventParamListener<T>>();

    public void Raise(T param)
    {
        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            listeners[i].OnDataEventRaised(param);
        }
    }

    public void RegisterListener(IDataEventParamListener<T> listener)
    {
        if (!listeners.Contains(listener))
        {
            listeners.Add(listener);
        }
    }

    public void UnregisterListener(IDataEventParamListener<T> listener)
    {
        if (listeners.Contains(listener))
        {
            listeners.Remove(listener);
        }
    }

    public override List<string> RegisteredListenersIds()
    {
        List<string> listenersIds = new List<string>();
        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            listenersIds.Add(listeners[i].GetListenerId());
        }
        return listenersIds;
    }
}