﻿using UnityEngine;

[CreateAssetMenu(fileName = "DataEventColor", menuName = "DataEvents/Color")]
public class SO_DataEventColor : SO_DataEventParamBase<Color> {}
