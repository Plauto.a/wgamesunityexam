﻿using UnityEngine;

[CreateAssetMenu(fileName = "DataEventMonoBehaviour", menuName = "DataEvents/MonoBehaviour")]
public class SO_DataEventMonoBehaviour : SO_DataEventParamBase<MonoBehaviour> {}
