﻿using UnityEngine;

[CreateAssetMenu(fileName = "DataEventString", menuName = "DataEvents/String")]
public class SO_DataEventString : SO_DataEventParamBase<string> {}
