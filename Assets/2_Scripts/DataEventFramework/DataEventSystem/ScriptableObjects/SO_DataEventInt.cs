﻿using UnityEngine;

[CreateAssetMenu(fileName = "DataEventInt", menuName = "DataEvents/Int")]
public class SO_DataEventInt : SO_DataEventParamBase<int> {}
