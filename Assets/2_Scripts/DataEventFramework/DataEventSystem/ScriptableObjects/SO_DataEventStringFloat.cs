﻿using UnityEngine;

[CreateAssetMenu(fileName = "DataEventStringFloat", menuName = "DataEvents/String+Float")]
public class SO_DataEventStringFloat : SO_DataEventTwoParamBase<string, float> { }
