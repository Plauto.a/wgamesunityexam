﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDataEventListener : IDataEventParamListenerID
{
    void OnDataEventRaised();
}
