﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDataEventParamListenerID
{
    string GetListenerId();
}
