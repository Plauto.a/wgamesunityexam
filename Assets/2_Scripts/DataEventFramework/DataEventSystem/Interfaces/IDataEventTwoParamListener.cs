﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDataEventTwoParamListener<T1, T2> : IDataEventParamListenerID
{
    void OnDataEventRaised(T1 param, T2 param2);
}
