﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDataEventParamListener<T> : IDataEventParamListenerID
{
    void OnDataEventRaised(T param);
}
